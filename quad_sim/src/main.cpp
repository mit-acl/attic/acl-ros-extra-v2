// Global includes
#include <iostream>

// Local includes
#include "QuadSim.h"

int main(int argc, char* argv[])
{
    ros::init(argc, argv, "sim");
    ros::NodeHandle n;

    double pub_dt, sim_dt;
    ros::param::param<double>("~pub_dt",pub_dt,0.01);
    ros::param::param<double>("~sim_dt",sim_dt,0.001);

    // Check namespace to find name of quad
    // Check for proper renaming of node namespace
    std::string quad_name = ros::this_node::getNamespace();
    quad_name.erase(std::remove(quad_name.begin(), quad_name.end(), '/'), quad_name.end());
    if (quad_name.empty()) {
        ROS_ERROR("Error :: You should be using a launch file to specify the "
                  "node namespace!\n");
        return (-1);
    }
    // Initialize vehicle
    QuadSim quad(quad_name,sim_dt);

    //## Welcome screen
    ROS_INFO("\n\nStarting ROS quad Dynamics code for for %s...\n\n\n",
             ros::this_node::getNamespace().c_str());

    acl::sQuadState initState = quad.getInitialState();

    // process command line arguments
    if (argc > 2) {
        // these should be position intializations
        initState.pos[0] = atof(argv[1]); // x
        initState.pos[1] = atof(argv[2]); // y
    }
    if (argc > 3)
        initState.pos[2] = atof(argv[3]); // z

    quad.dynamics.setInitialState(initState);

    // Set up state publishers
    quad.state_pub = n.advertise<acl_msgs::ViconState>("vicon", 1);
    quad.ahrs_pub = n.advertise<acl_msgs::QuadAHRS>("ahrs", 1);
    quad.wind_pub = n.advertise<acl_msgs::FloatStamped>("wind", 1);

    // Set listener callbacks for command data
    ros::Subscriber sub_cmd = n.subscribe("cmds", 1, &QuadSim::cmdCB, &quad);

    // initialize main controller loop
    quad.controller_timer = n.createTimer(ros::Duration(sim_dt), &QuadSim::runSim, &quad);

    quad.broadcast_timer = n.createTimer(ros::Duration(pub_dt), &QuadSim::broadcastState, &quad);

    // run the code
    ros::spin();

    return 0;
}
