/*
 * QuadSim.cpp
 *
 *  Created on: Jun 11, 2014
 *      Author: mark
 */

#include "QuadSim.h"

QuadSim::QuadSim(const std::string& name, double dt): name_(name), dt_(dt)
{
	setParams();
	dynamics.setInitialState(getInitialState());
	qft.setParam(dynamics.getParams());
	cmd.AttCmd = 0;
	cmd.Q = Eigen::Quaterniond::Identity();
	cmd.rate = Eigen::Vector3d::Zero();
	cmd.throttle = 0;

}

QuadSim::~QuadSim()
{
	// TODO Auto-generated destructor stub
}

/**
 * @return An initial state struct with all zero elements
 */
acl::sQuadState QuadSim::getInitialState(void)
{
	acl::sQuadState s;
	s.Q = Eigen::Quaterniond::Identity();
	s.pos.setZero();
	s.vel.setZero();
	s.rate.setZero();

	return s;
}

/**
 * Function that sets params for sim
 */
void QuadSim::setParams(){
	acl::sQuadParam sim_params;
    ///< Vehicle mass (kg)
    ros::param::param<double>("~mass",sim_params.m,1.0);
    ///< Mass Moment of Inertia
    ros::param::param<double>("~Jx",sim_params.J(0,0),0.01);
    ros::param::param<double>("~Jy",sim_params.J(1,1),0.01);
    ros::param::param<double>("~Jz",sim_params.J(2,2),0.01);
    ///< Arm length (m)
    ros::param::param<double>("~arm_length",sim_params.l,0.1);
    ///< drag coefficient
    ros::param::param<double>("~drag",sim_params.drag,0.001);
    ///< motor scalling
    ros::param::param<double>("~motor_scale",sim_params.motor_scale,0.03);
    ///< body drag coefficient
    ros::param::param<double>("~body_drag",sim_params.body_drag,0.0);

    dynamics.setParams(sim_params);
}

/**
 * Main function for running the simulation at a fixed rate of 1/DT Hz
 * @param e
 */
void QuadSim::runSim(const ros::TimerEvent& e)
{

	// run onboard controller
	Eigen::Vector4d motorcmd = obc.runController(state.Q, state.rate,
			cmd.Q, cmd.rate, cmd.throttle, cmd.AttCmd);

	// get resulting forces and moments
	geometry_msgs::Wrench wrench = qft.getTotalFT(motorcmd, state);

	// integrate forward dynamics and send state
	double F[3] = {wrench.force.x, wrench.force.y, wrench.force.z};
	double M[3] = {wrench.torque.x, wrench.torque.y, wrench.torque.z};
	dynamics.setThrustMoments(F, M);
	dynamics.integrateStep(dt_);
	state = dynamics.getState();

}


void QuadSim::cmdCB(const acl_msgs::QuadCntrl& cmd)
{
	this->cmd.Q = Eigen::Quaterniond(cmd.pose.orientation.w, cmd.pose.orientation.x,
			cmd.pose.orientation.y, cmd.pose.orientation.z);
	this->cmd.rate = Eigen::Vector3d(cmd.twist.angular.x, cmd.twist.angular.y,
			cmd.twist.angular.z);
	this->cmd.throttle = cmd.throttle;
	this->cmd.AttCmd = cmd.att_status; // TODO:fix this -- make part of quadCmd message
}

void QuadSim::broadcastTimeout(const ros::WallTimerEvent& e)
{
	 // If this fires, stop the state timer broadcast
	//broadcastTimer.stop();
}

/**
 * Broadcast the state data in a ros message
 * @param state CarState of data that should be broadcast
 */
void QuadSim::broadcastState(const ros::TimerEvent& e)
{
	geometry_msgs::Pose pose;
	pose.position.x = state.pos(0);
	pose.position.y = state.pos(1);
	pose.position.z = state.pos(2);
	pose.orientation.w = state.Q.w();
	pose.orientation.x = state.Q.x();
	pose.orientation.y = state.Q.y();
	pose.orientation.z = state.Q.z();

	acl_msgs::QuadAHRS ahrs;
	ahrs.header.stamp = ros::Time::now();
	ahrs.header.frame_id = name_;
	
	ahrs.att.w = state.Q.w();
	ahrs.att.x = state.Q.x();
	ahrs.att.y = state.Q.y();
	ahrs.att.z = state.Q.z();
	ahrs.att_meas.w = state.Q.w();
	ahrs.att_meas.x = state.Q.x();
	ahrs.att_meas.y = state.Q.y();
	ahrs.att_meas.z = state.Q.z();	

	double r, p, y;

	tf::Quaternion tf_q(ahrs.att.x,ahrs.att.y,ahrs.att.z,ahrs.att.w);

	tf::Matrix3x3(tf_q).getRPY(r, p, y);
	ahrs.rpy.x = r; // roll
	ahrs.rpy.y = p; // pitch
	ahrs.rpy.z = y; // yaw

	tf::Quaternion tf_q_meas(ahrs.att_meas.x,ahrs.att_meas.y,ahrs.att_meas.z,ahrs.att_meas.w);

	tf::Matrix3x3(tf_q_meas).getRPY(r, p, y);
	ahrs.rpy_meas.x = r; // roll
	ahrs.rpy_meas.y = p; // pitch
	ahrs.rpy_meas.z = y; // yaw

	geometry_msgs::Twist twist;
	twist.linear.x = state.vel(0);
	twist.linear.y = state.vel(1);
	twist.linear.z = state.vel(2);
	twist.angular.x = state.rate(0);
	twist.angular.y = state.rate(1);
	twist.angular.z = state.rate(2);

	// publish results
	geometry_msgs::PoseStamped pose_stamped;
	// pull off first and last '/' characters
	pose_stamped.header.frame_id = ros::this_node::getNamespace().substr(1, ros::this_node::getNamespace().size()-1);
	pose_stamped.header.stamp = ros::Time::now();
	pose_stamped.pose = pose;

	geometry_msgs::TwistStamped twist_stamped;
	twist_stamped.header = pose_stamped.header;
	twist_stamped.twist = twist;

	acl_msgs::FloatStamped w_mag;
	w_mag.header = pose_stamped.header;
	w_mag.data = dynamics.dy;


	acl_msgs::ViconState vicon_state_msg;
	vicon_state_msg.header.stamp = pose_stamped.header.stamp;
	vicon_state_msg.header.frame_id = "vicon";
	vicon_state_msg.pose = pose;
	vicon_state_msg.twist = twist;
	vicon_state_msg.has_pose = true;
	vicon_state_msg.has_twist = true;
	vicon_state_msg.has_accel = false;

	// pose_pub.publish(pose_stamped);
	// twist_pub.publish(twist_stamped);
	state_pub.publish(vicon_state_msg);
	ahrs_pub.publish(ahrs);	
	wind_pub.publish(w_mag);	

	// Pure translation transform
	tf::Transform tf_trans;
	tf_trans.setOrigin(tf::Vector3(vicon_state_msg.pose.position.x,vicon_state_msg.pose.position.y,vicon_state_msg.pose.position.z));
	tf::Quaternion q_none(0.0,0.0,0.0,1.0);
	tf_trans.setRotation(q_none);
	transform_broadcaster_.sendTransform(tf::StampedTransform(tf_trans, pose_stamped.header.stamp, "vicon", name_ + "/trans"));

	// Full transform
	tf::Transform tf_rot;
	tf_rot.setOrigin(tf::Vector3(0.0,0.0,0.0));
	tf::Quaternion q(vicon_state_msg.pose.orientation.x,vicon_state_msg.pose.orientation.y,vicon_state_msg.pose.orientation.z,vicon_state_msg.pose.orientation.w);
	tf_rot.setRotation(q);
	transform_broadcaster_.sendTransform(tf::StampedTransform(tf_rot, pose_stamped.header.stamp, name_ + "/trans", name_));
}
