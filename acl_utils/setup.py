#!/usr/bin/env python
from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(
    packages=['acl_utils'],
    package_dir={'': 'include'},
)

setup(**setup_args)


# setup(name='acl_utils',
#       version='0.1',
#       description='Common python functions used in the ACL',
#       author='Mark Cutler',
#       author_email='markjcutler [at] gmail [dot] com',
#       packages=['aclpy'],
#       )
