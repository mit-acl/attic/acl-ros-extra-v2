#!/usr/bin/env python
import numpy as np
import rosbag
import matplotlib.pyplot as pl
from mpl_toolkits.mplot3d import Axes3D
import roslib

def quat2angle(q0,q1,q2,q3):
    roll = (np.arctan2(2*(q0*q1+q2*q3), 1-2*(q1*q1+q2*q2)))
    pitch = (np.arcsin(2*(q0*q2-q3*q1)))
    yaw = (np.arctan2(2*(q0*q3+q1*q2), 1-2*(q2*q2+q3*q3)))

    return [roll, pitch, yaw]

# Bag to be processed
file_name = 'flight3.bag'
bag = rosbag.Bag(file_name)

i = 0

# Topics to be plotted
topics=['/SQ01/cmds']

for topic, msg, t in bag.read_messages(topics):
	if topic == topics[0]:
		if i == 0:
			time_bias = msg.header.stamp.to_sec()
			t_c = np.array([msg.header.stamp.to_sec()])
			pose_des = np.array([msg.pose.position.x, msg.pose.position.y, msg.pose.position.z])
			att_des = np.array([msg.pose.orientation.w,msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z])
			vel_des = np.array([msg.twist.linear.x, msg.twist.linear.y, msg.twist.linear.z])
			rate_des = np.array([msg.twist.angular.x, msg.twist.angular.y, msg.twist.angular.z])

			pose = np.array([msg.pose_actual.position.x, msg.pose_actual.position.y, msg.pose_actual.position.z])
			att = np.array([msg.pose_actual.orientation.w,msg.pose_actual.orientation.x, msg.pose_actual.orientation.y, msg.pose_actual.orientation.z])
			vel = np.array([msg.twist_actual.linear.x, msg.twist_actual.linear.y, msg.twist_actual.linear.z])
			rate = np.array([msg.twist_actual.angular.x, msg.twist_actual.angular.y, msg.twist_actual.angular.z])

			d2_c = np.array([msg.accel.x,msg.accel.y,msg.accel.z])
			d2_fb = np.array([msg.accel_fb.x,msg.accel_fb.y,msg.accel_fb.z])
			d3_c = np.array([msg.jerk.x,msg.jerk.y,msg.jerk.z])
			d3_fb = np.array([msg.jerk_fb.x,msg.jerk_fb.y,msg.jerk_fb.z])
			throttle = np.array([msg.throttle])
			int_x = np.array([msg.pos_integrator.x])
			int_y = np.array([msg.pos_integrator.y])
			int_z = np.array([msg.pos_integrator.z])
			
			b = np.array([msg.b])
			sx = np.array([msg.s.x])
			sy = np.array([msg.s.y])
			sz = np.array([msg.s.z])
			cx = np.array([msg.c.x])
			cy = np.array([msg.c.y])
			cz = np.array([msg.c.z])

			phix = np.array([msg.phi.x])
			phiy = np.array([msg.phi.y])
			phiz = np.array([msg.phi.z])

			i+=1
		else:
			t_c = np.vstack((t_c,msg.header.stamp.to_sec()))
			pose_des = np.vstack((pose_des,[msg.pose.position.x, msg.pose.position.y, msg.pose.position.z]))
			att_des = np.vstack((att_des,[msg.pose.orientation.w,msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z]))
			vel_des = np.vstack((vel_des,[msg.twist.linear.x, msg.twist.linear.y, msg.twist.linear.z]))
			rate_des = np.vstack((rate_des,[msg.twist.angular.x, msg.twist.angular.y, msg.twist.angular.z]))
			
			pose = np.vstack((pose,[msg.pose_actual.position.x, msg.pose_actual.position.y, msg.pose_actual.position.z]))
			att = np.vstack((att,[msg.pose.orientation.w,msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z]))
			vel = np.vstack((vel,[msg.twist.linear.x, msg.twist.linear.y, msg.twist.linear.z]))
			rate = np.vstack((rate,[msg.twist.angular.x, msg.twist.angular.y, msg.twist.angular.z]))

			d2_c = np.vstack((d2_c,[msg.accel.x,msg.accel.y,msg.accel.z]))
			d2_fb = np.vstack((d2_fb,[msg.accel_fb.x,msg.accel_fb.y,msg.accel_fb.z]))
			d3_c = np.vstack((d3_c,[msg.jerk.x,msg.jerk.y,msg.jerk.z]))
			d3_fb = np.vstack((d3_fb,[msg.jerk_fb.x,msg.jerk_fb.y,msg.jerk_fb.z]))
			throttle = np.vstack((throttle,msg.throttle))
			int_x = np.vstack((int_x,msg.pos_integrator.x))
			int_y = np.vstack((int_y,msg.pos_integrator.y))
			int_z = np.vstack((int_z,msg.pos_integrator.z))

			b = np.vstack((b,msg.b))
			sx = np.vstack((sx,msg.s.x))
			sy = np.vstack((sy,msg.s.y))
			sz = np.vstack((sz,msg.s.z))
			cx = np.vstack((cx,msg.c.x))
			cy = np.vstack((cy,msg.c.y))
			cz = np.vstack((cz,msg.c.z))

			phix = np.vstack((phix,msg.phi.x))
			phiy = np.vstack((phiy,msg.phi.y))
			phiz = np.vstack((phiz,msg.phi.z))
	

t_c = t_c - time_bias

roll_des, pitch_des, yaw_des = quat2angle(att_des[:,0],att_des[:,1],att_des[:,2],att_des[:,3])
roll, pitch, yaw = quat2angle(att[:,0],att[:,1],att[:,2],att[:,3])

#### Write to .txt file ####
# e = pose - pose_des

# tfile = open("data.txt",'w')
# tfile.write("t ex ey ez sx sy sz cx cy cz phix phiy phiz b\n")

# for i in range(len(t_c)):
# 	tfile.write("%f %f %f %f %f %f %f %f %f %f %f %f %f %f \n" % (t_c[i], e[i,0], e[i,1], e[i,2], sx[i], sy[i], sz[i],  cx[i], cy[i], cz[i], phix[i], phiy[i], phiz[i], b[i]))
# tfile.close()

#### Plot data ####
# Position
pl.figure(1)
ax1 = pl.subplot(311)
pl.title("Position")
pl.plot(t_c,pose_des[:,0],t_c,pose[:,0],'r')
pl.grid()
pl.legend(['Command','Actual'])
pl.ylabel("x [m]")
pl.subplot(312,sharex=ax1)
pl.plot(t_c,pose_des[:,1],t_c,pose[:,1],'r')
pl.grid()
pl.ylabel("y [m]")
pl.subplot(313,sharex=ax1)
pl.plot(t_c,pose_des[:,2],t_c,pose[:,2],'r')
pl.grid()
pl.ylabel("z [m]")
pl.xlabel("Time [s]")

# Velocity
pl.figure(2)
pl.subplot(311,sharex=ax1)
pl.title("Velocity")
pl.plot(t_c,vel_des[:,0],t_c,vel[:,0],'r')
pl.grid()
pl.legend(['Command','Actual'])
pl.ylabel("x [m/s]")
pl.subplot(312,sharex=ax1)
pl.plot(t_c,vel_des[:,1],t_c,vel[:,1],'r')
pl.grid()
pl.ylabel("y [m/s]")
pl.subplot(313,sharex=ax1)
pl.plot(t_c,vel_des[:,2],t_c,vel[:,2],'r')
pl.grid()
pl.ylabel("z [m/s]")
pl.xlabel("Time [s]")

# Attitude
pl.figure(3)
pl.subplot(311,sharex=ax1)
pl.title("Attitude")
pl.plot(t_c,roll_des*180/np.pi,t_c,roll*180/np.pi,'r')
pl.grid()
pl.legend(['Command','Actual'])
pl.ylabel("Roll [deg]")
pl.subplot(312,sharex=ax1)
pl.plot(t_c,pitch_des*180/np.pi,t_c,pitch*180/np.pi,'r')
pl.grid()
pl.ylabel("Pitch [deg]")
pl.subplot(313,sharex=ax1)
pl.plot(t_c,yaw_des*180/np.pi,t_c,yaw*180/np.pi,'r')
pl.grid()
pl.ylabel("yaw [deg]")
pl.xlabel("Time [s]")

# Attitude Rate
pl.figure(4)
pl.subplot(311,sharex=ax1)
pl.title("Attitude Rate")
pl.plot(t_c,rate_des[:,0]*180/np.pi,t_c,rate[:,0]*180/np.pi,'r')
pl.grid()
pl.legend(['Command','Actual'])
pl.ylabel("Roll Rate [deg/s]")
pl.subplot(312,sharex=ax1)
pl.plot(t_c,rate_des[:,1]*180/np.pi,t_c,rate[:,1]*180/np.pi,'r')
pl.grid()
pl.ylabel("Pitch Rate [deg/s]")
pl.subplot(313,sharex=ax1)
pl.plot(t_c,rate_des[:,2]*180/np.pi,t_c,rate[:,2]*180/np.pi,'r')
pl.grid()
pl.ylabel("Yaw Rate [deg/s]]")
pl.xlabel("Time [s]")

# Acceleration
pl.figure(5)
pl.subplot(311,sharex=ax1)
pl.title("Acceleration")
pl.plot(t_c,d2_c[:,0],t_c,d2_fb[:,0],'r',t_c,d2_c[:,0]+d2_fb[:,0],'k')
pl.grid()
pl.legend(['Feedforward','Feedback','Summation'])
pl.ylabel("x [m/s^2]")
pl.subplot(312,sharex=ax1)
pl.plot(t_c,d2_c[:,1],t_c,d2_fb[:,1],'r',t_c,d2_c[:,1]+d2_fb[:,1],'k')
pl.grid()
pl.ylabel("y [m/s^2]")
pl.subplot(313,sharex=ax1)
pl.plot(t_c,d2_c[:,2],t_c,d2_fb[:,2],'r',t_c,d2_c[:,2]+d2_fb[:,2],'k')
pl.grid()
pl.ylabel("z [m/s^2]")
pl.xlabel("Time [s]")

# Jerk
pl.figure(6)
pl.subplot(311,sharex=ax1)
pl.title("Jerk")
pl.plot(t_c,d3_c[:,0],t_c,d3_fb[:,0],'r',t_c,d3_c[:,0]+d3_fb[:,0],'k')
pl.grid()
pl.legend(['Feedforward','Feedback','Summation'])
pl.ylabel("x [m/s^3]")
pl.subplot(312,sharex=ax1)
pl.plot(t_c,d3_c[:,1],t_c,d3_fb[:,1],'r',t_c,d3_c[:,1]+d3_fb[:,1],'k')
pl.grid()
pl.ylabel("y [m/s^3]")
pl.subplot(313,sharex=ax1)
pl.plot(t_c,d3_c[:,2],t_c,d3_fb[:,2],'r',t_c,d3_c[:,2]+d3_fb[:,2],'k')
pl.grid()
pl.ylabel("z [m/s^3]")
pl.xlabel("Time [s]")

# Position and Velocity Integrator Values
pl.figure(7)
pl.subplot(211,sharex=ax1)
pl.plot(t_c,throttle)
pl.ylabel("Throttle")
pl.grid()
pl.subplot(212,sharex=ax1)
pl.plot(t_c,int_x,t_c,int_y,t_c,int_z)
pl.grid()
pl.legend(['x','y','z'])
pl.ylabel("Position Integrator Values")
pl.xlabel("Time [s]")

# 3D plot
fig = pl.figure(8)
ax = fig.gca(projection='3d')
com,=ax.plot(pose_des[:,0],pose_des[:,1],pose_des[:,2])
ax.set_xlabel('x [m]')
ax.set_ylabel('y [m]')
ax.set_zlabel('z [m]')
act,=ax.plot(pose[:,0],pose[:,1],pose[:,2],'r')
ax.legend([com,act],['Desired','Actual'])


pl.show()
