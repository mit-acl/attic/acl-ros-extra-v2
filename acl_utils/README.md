ACL_UTILS
============

-------------------------------------------------------------------------------
### Authors:
	Mark Cutler -- cutlerm@mit.edu
	
-------------------------------------------------------------------------------
### Project description

Common shared libraries for c++ and python development.  The project creates a
dynamically linked library called libacl_utils.so and a python module called aclpy.  The included
install script builds the libraries and adds the compiled  files and
relevant header files to the appropriate locations so the functions
can be called from c++ and python applications.

Any functions/classes that are used across multiple projects in ACL
code should be included in this library.

-------------------------------------------------------------------------------
### Dependencies:

- doxygen, libbluetooth-dev, libpcl-1.7-all

All the dependencies should be installed automatically using the install script.

-------------------------------------------------------------------------------
### Installation

Note: This has only been tested in Ubuntu 14.04.

The root folder contains an executable script called install.sh that
will build the release version of the project and copy the library
into /usr/local/lib/ and the header files into
/usr/local/include/acl/.  The script also runs ldconfig to update the
computer's library paths.

To install the library on Ubuntu (and probably any version of Linux),
open a terminal, navigate to the root directory of the folder, and
type

    sudo ./install.sh

Assuming you get no errors, the library can be used by including the
corresponding header files and linking to the library.

#### Documentation

All the c code in the library is (or at least should be) documented with
doxygen.  The documentation is generated when the install script is run.
The compiled documentation can be found
in docs/html/index.html.

#### Example

  A simple
example file is below:

~~~
/******** EXAMPLE  **************/
#include <iostream>
#include "acl_utils/utils.hpp"

int main()
{
	double x = 3.0;
	double xsat = acl::saturate(x, 2.0, -2.0);
	std::cout << "Original: " << x << std::endl;
	std::cout << "Saturated: " << xsat << std::endl;
	return 0;
}
/******** End Example ***********/
~~~

This example can be complied with

    g++ -Wall sampleProject.cpp -lacl_utils -o sampleProject

-------------------------------------------------------------------------------
### Additional Notes

#### Using the library with Eclipse projects

To use the library with an eclipse project, right click on your
project name and select properties.  Under "C/C++ Build" select
"Settings". Navigate to "Libraries" under "GCC C++ Linker" in the
"Tool Settings" tab.  Add the library "acl_utils" in the "Libraries
(-l)" section.  Hit OK and your program should link to the library.


#### Adding to / Modifying the library

Adding to and modifying the contents of the library is encouraged.  The library
is compiled using cmake.  You just need to add your .cpp files to the CMakeLists.txt
file.

Please document anything you add and make sure the function works
before pushing to the server.   Also, please keep all of your functions and
classes in the acl namespace for consistency.

#### Using the library with ROS projects

Linking to the library is done with in the CMakeLists.txt file.  After
the rosbuild_add_executable(prog_name src_files) call, add the
following line.

    target_link_libraries(executable ${catkin_LIBRARIES} acl_utils)


-------------------------------------------------------------------------------
### References

Notes on setting library path:
- http://www.cyberciti.biz/faq/linux-setting-changing-library-path/

Building libraries with Linux: 
- http://linuxtortures.blogspot.com/2012/02/shared-libraries-with-eclipse.html
- http://linuxtortures.blogspot.com/2012/03/shared-libraries-with-eclipse-on-8664.html