/*!
 * \file udpPort.hpp
 *
 * Opens, reads from, and writes to a UDP port
 *
 *  Created on: Mar 15, 2013
 *      Author: Buddy Michini
 *
 */

#ifndef UDPPORT_H_
#define UDPPORT_H_

#include <iostream>
#include <string>
#include <termios.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

namespace acl
{
class UDPPort
{

    bool broadcast;
    bool multicast;

    char localIP[128];
    char multiIP[128];

    int socketTx;
    int socketRx;
    int socketMc;

    struct sockaddr_in addressTx;
    struct sockaddr_in addressRx;
    struct sockaddr_in addressMc;

    ip_mreq mc_req;

public:
    UDPPort()
    {
    }
    ;
    ~UDPPort();
    int udpInit(bool bcast, char* udpIP, int RxPort, int TxPort);
    int udpInit(char* udpIP, char* multicastIP, int portnum);
    int udpSend(unsigned char* pkt, unsigned int);
    int udpReceive(char* buffer, int len);
    void udpClose();
};

} // end namespace acl
;
#endif
