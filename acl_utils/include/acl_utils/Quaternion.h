/*
 * Quaternion.h
 *
 *  Created on: May 16, 2014
 *      Author: mark
 */

#ifndef QUATERNION_H_
#define QUATERNION_H_

namespace acl
{

class Quaternion
{
public:
    Quaternion();
    Quaternion(double w);
    Quaternion(double w, double x, double y, double z);
    virtual ~Quaternion();

    void setW(double w)
    {
        this->w = w;
    }
    ;
    void setX(double x)
    {
        this->x = x;
    }
    ;
    void setY(double y)
    {
        this->y = y;
    }
    ;
    void setZ(double z)
    {
        this->z = z;
    }
    ;

    double getW()
    {
        return this->w;
    }
    ;
    double getX()
    {
        return this->x;
    }
    ;
    double getY()
    {
        return this->y;
    }
    ;
    double getZ()
    {
        return this->z;
    }
    ;

    Quaternion operator+(const Quaternion& other);
    Quaternion operator*(const Quaternion& other);
    Quaternion operator*(const double scalar);
    Quaternion conj();

private:
    double w, x, y, z;
};

} /* namespace acl */
#endif /* QUATERNION_H_ */
