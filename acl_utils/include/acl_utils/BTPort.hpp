/*!
 * \file BTPort.hpp
 *
 * Opens, reads from, and writes to a Bluetooth device
 *
 *  Created on: Mar 15, 2013
 *      Author: Buddy Michini
 *
 */

#ifndef BTPORT_H_
#define BTPORT_H_

#include <string>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>
#include <bluetooth/l2cap.h>

/// Maximum serial port buffer size
#define SER_BUF_SZ 5000

namespace acl
{
class BTPort
{

    const char* mac;
    char buffer[2048];
    int s;

public:
    int BTInit(std::string macaddr);
    int BTSend(char* pkt, int len);
    uint8_t BTReceiveByte();
    void BTClose();
};
}
;

#endif
