/*!
 * \file UpuccDynamics.hpp
 *
 * Very simple kinematic model of a skid steer vehicle taken from
 * appendix A here: http://acl.mit.edu/papers/DesarajuSM.pdf
 *
 *  Created on: May 9, 2014
 *      Author: Mark Cutler
 *     Contact: markjcutler@gmail.com
 *
 */

#ifndef UPUCCDYNAMICS_HPP_
#define UPUCCDYNAMICS_HPP_

// Global includes
#include <valarray>
#include <boost/bind.hpp>
#include <random>
#include <chrono>

// Local includes
#include "../utils.hpp"

namespace acl
{

const unsigned int STATE_LENGTH = 3;

/// parameter struct
struct sUpuccParam
{
    double Lw; ///< Wheel base width
};

/// state struct
struct sUpuccState
{
    double x, y; ///< Inertial positions
    double psi; ///< Yaw
};

/**
 *  Upucc dynamics
 */
class UpuccDynamics
{
public:
    UpuccDynamics();
    virtual ~UpuccDynamics();

    void setParamStruct(struct sUpuccParam param);
    void setV(double vL, double vR);
    void setInitialState(struct sUpuccState initState);

    struct sUpuccState getState(void);
    struct sUpuccParam getParams(void);

    void integrateStep(double dt);

private:
    std::string name; ///< car name
    struct sUpuccState state; ///< state
    struct sUpuccParam param; ///< param struct

    double vL; ///< left wheel speed
    double vR; ///< right wheel speed

    double simTime; ///< simulation time

    std::valarray<double> dynamics(double dt, std::valarray<double> s);
};

} /* namespace acl */
#endif /* UPUCCDYNAMICS_HPP_ */
