/*!
 * \file BTPort.cpp
 *
 * Sets up a wifly device
 *
 *  Created on: May 7, 2015
 *      Author: Mark Cutler
 *
 */

#include "wifly.hpp"

namespace acl
{

void *listen(void *s)
{
    acl::SerialPort *ser = (acl::SerialPort*) s;

    while (1)
    {

        // listen to serial port
        uint8_t c = ser->spReceiveSingle();
        printf("%c", c);
    }
    return NULL;
}

bool wifly::set_host_ip(std::string host, int port,
        std::vector<int> expected_address)
{
    try
    {
        boost::asio::io_service io_service;
        // resolve the host name and port number to an iterator that can be used to connect to the server
        tcp::resolver resolver(io_service);
        tcp::resolver::query query(host.c_str(), std::to_string(port).c_str());
        tcp::resolver::iterator iterator = resolver.resolve(query);
        // define an instance of the main class of this program
        telnet_client c(io_service, iterator);
        // run the IO service as a separate thread, so the main thread can block on standard input
        boost::thread t(
                boost::bind(&boost::asio::io_service::run, &io_service));

        std::string acl_raven_ip;

        if (get_acl_ip(expected_address, acl_raven_ip))
        {

            sleep(1);
            c.write("$$$", 0);
            usleep(300 * 1000); // sleep for 300 milliseconds
            c.write("set ip host ", 0);
            c.write(acl_raven_ip, 1);
            usleep(20 * 1000); // sleep for 20 milliseconds
            c.write("save", 1);
            usleep(900 * 1000); // sleep for 50 milliseconds
            c.write("get ip", 1);
            usleep(900 * 1000);
            c.write("exit", 1);
            sleep(1);
        }
        else
        {
            std::cout << "ERROR: could not find ACL RAVEN ip addres"
                    << std::endl;
            c.close(); // close the telnet client connection
            t.join(); // wait for the IO service thread to close
            return false;
        }

        c.close(); // close the telnet client connection
        t.join(); // wait for the IO service thread to close
        return true;
    } catch (exception& e)
    {
        cerr << "Exception: " << e.what() << "\n";
        return false;
    }

}

bool wifly::get_acl_ip(std::vector<int> expected_address,
        std::string &acl_raven_ip)
{
    int subnet_length = expected_address.size();

    struct ifaddrs * ifAddrStruct = NULL;
    struct ifaddrs * ifa = NULL;
    void * tmpAddrPtr = NULL;

    getifaddrs(&ifAddrStruct);

    bool found_acl_ip = true;
    printf("\n\n\nLooking for all IP addresses on this machine\n\n");
    printf("WARNING: this script will return the FIRST ip address matching\n");
    printf(
            "    the acl ip address pattern. If there are multiple acl addresses,\n");
    printf("    this script will need to be modified.\n\n\n");
    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (!ifa->ifa_addr)
        {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET)
        { // check it is IP4
            // is a valid IP4 Address
            tmpAddrPtr = &((struct sockaddr_in *) ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);

            // parse out the ip4 address into a series of four numbers
            std::string ab(addressBuffer);
            std::istringstream iss(ab);
            std::string token;
            int cnt = 0;
            found_acl_ip = true;
            while (std::getline(iss, token, '.'))
            {
                if (!token.empty() and cnt < subnet_length)
                {
                    int add_num = atoi(token.c_str());
                    if (add_num != expected_address[cnt])
                        found_acl_ip = false;
                    cnt++;
                }
            }

            if (found_acl_ip)
            {
                acl_raven_ip = addressBuffer;
                break;
            }

        } //  else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
          //     // is a valid IP6 Address
          //     tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
          //     char addressBuffer[INET6_ADDRSTRLEN];
          //     inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
          //     printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
          // }
    }
    if (ifAddrStruct != NULL)
        freeifaddrs(ifAddrStruct);

    if (found_acl_ip)
        printf("\n\n\nFound the acl-raven-ip: %s\n\n\n", acl_raven_ip.c_str());
    else
        printf("\n\n\nNo acl-raven-ip address found\n\n\n");
    return found_acl_ip;
}

bool wifly::setup_wifly(std::string ssid, int device_port, int host_port,
        std::string host_ip, std::string device_ip, std::string password,
        std::string serial_port, int initial_baud_rate, int target_baud_rate)
{
    // setup configuration variables
    std::string remote = std::to_string(device_port);
    std::string local = std::to_string(host_port);

    acl::SerialPort ser;

    // setup serial port
    if (!ser.spInitialize(serial_port.c_str(), initial_baud_rate, true))
    {
        std::cout << "Serial port failed to open" << std::endl;
        return 0;
    }

    // setup listen thread
    pthread_t threads;
    if (pthread_create(&threads, NULL, listen, &ser))
        std::cout << "Serial listen thread failed to start" << std::endl;

    char com[100];
    int n = sprintf(com, "%s", "$$$");
    ser.spSend(com, n);
    sleep(1);

    n = sprintf(com, "%s", "factory RESET\r\n");
    ser.spSend(com, n);

    n = sprintf(com, "%s", "save\r\n");
    ser.spSend(com, n);

    n = sprintf(com, "%s", "reboot\r\n");
    ser.spSend(com, n);
    sleep(1);

    // close serial port and reopen at 9600
    ser.spClose();
    // setup serial port
    if (!ser.spInitialize(serial_port.c_str(), 9600, true))
    {
        std::cout << "Serial port failed to open" << std::endl;
        return 0;
    }

    n = sprintf(com, "%s", "$$$");
    ser.spSend(com, n);
    sleep(1);

    n = sprintf(com, "%s", "set wlan auth 4\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s", "set wlan join 1\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s%s%s", "set wlan ssid ", ssid.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s%s%s", "set wlan phrase ", password.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s", "set ip protocol 3\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s", "set ip dhcp 0\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s%s%s", "set ip host ", host_ip.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s%s%s", "set ip remote ",
            std::to_string(device_port).c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s%s%s", "set ip address ", device_ip.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s%s%s", "set ip localport ",
            std::to_string(host_port).c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s%s%s", "join ", ssid.c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s%s%s", "set u b ",
            std::to_string(target_baud_rate).c_str(), "\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s", "save\r\n");
    ser.spSend(com, n);
    sleep(1);
    n = sprintf(com, "%s", "reboot\r\n");
    ser.spSend(com, n);
    sleep(1);

    return true;
}
}
;
