/*!
 * \file CarPoleDynamics.cpp
 *
 * @todo Brief file description
 *
 *  Created on: May 28, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#include "CartPoleDynamics.hpp"

namespace acl
{

CartPoleDynamics::CartPoleDynamics()
{

    // Initialize all variables
    simTime = 0.0;

    // Set default initial parameters
    param.m = 0.15;
    param.mc = 1.0;
    param.l = 0.75;
    param.muc = 100 * 0.0005;
    param.mup = 100 * 0.000002;

    // Set default initial state
    state.x = 0.0;
    state.dx = 0.0;
    state.theta = 0.0;
    state.dtheta = 0.0;

    force = 0.0;
}

CartPoleDynamics::~CartPoleDynamics()
{
    // TODO Auto-generated destructor stub
}

/**
 *
 * @return
 */
struct sCartPoleState CartPoleDynamics::getState(void)
{
    return this->state;
}

/**
 *
 * @return
 */
struct sCartPoleParam CartPoleDynamics::getParams(void)
{
    return this->param;
}

/**
 * Initialize state and reset time to zero
 * @param initState Initial state value
 */
void CartPoleDynamics::setInitialState(struct sCartPoleState initState)
{
    state = initState;
    simTime = 0;
}

/**
 * Set force (acting on cart)
 * @param F Force in Newtons
 */
void CartPoleDynamics::setForce(double F)
{
    force = F;
}

/**
 * Get force (acting on cart)
 * @return F Force in Newtons
 */
double CartPoleDynamics::getForce(void)
{
    return force;
}

/**
 * Integrate the system dynamics one time step
 * @param dt Time step
 */
void CartPoleDynamics::integrateStep(double dt)
{
    std::valarray<double> currentState(STATE_LENGTH);
    std::valarray<double> nextState(STATE_LENGTH);

    currentState[0] = state.x;
    currentState[1] = state.dx;
    currentState[2] = state.theta;
    currentState[3] = state.dtheta;

    // run the RK4 integration step
    nextState = acl::rk4(simTime, currentState, dt,
            boost::bind(&CartPoleDynamics::dynamics, this, _1, _2));
    simTime += dt;

    state.x = nextState[0];
    state.dx = nextState[1];
    state.theta = nextState[2];
    state.dtheta = nextState[3];
}

/**
 * System dynamics
 * @param dt Time step
 * @param s State
 * @return Derivative of state
 */
std::valarray<double> CartPoleDynamics::dynamics(double dt,
        std::valarray<double> s)
{

    // double x 	 	= s[0];
    double dx = s[1];
    double theta = s[2];
    double dtheta = s[3];

    // System dynamics
    double xdot = dx;
    double thetadot = dtheta;

    double tmp1 = param.l
            * (4.0 / 3.0
                    - param.m * cos(theta) * cos(theta) / (param.mc + param.m));
    double tmp2 = (-force - param.m * param.l * dtheta * dtheta * sin(theta)
            + param.muc * sgn(dx)) / (param.mc + param.m);
    double dthetadot = (GRAVITY * sin(theta) + cos(theta) * tmp2
            - param.mup * dtheta / (param.m * param.l)) / tmp1;
    double tmp3 = force
            + param.m * param.l
                    * (dtheta * dtheta * sin(theta) - dthetadot * cos(theta))
            - param.muc * sgn(dx);
    double dxdot = tmp3 / (param.mc + param.m);

    //	double xdot = dx;
    //	double tmp1 = 1/(param.mc + param.m*sin(theta)*sin(theta));
    //	double dxdot = tmp1*(force + param.m*param.l*dtheta*dtheta*sin(theta) -
    //param.m*GRAVITY*sin(theta)*cos(theta));
    //
    //	double thetadot = dtheta;
    //	double dthetadot = 1/param.l*(GRAVITY*sin(theta) - dxdot*cos(theta));

    std::valarray<double> out(STATE_LENGTH);
    out[0] = xdot;
    out[1] = dxdot;
    out[2] = thetadot;
    out[3] = dthetadot;

    return out;
}

} /* namespace acl */
