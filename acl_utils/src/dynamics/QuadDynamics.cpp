/*!
 * \file QuadDynamics.cpp
 *
 * @todo Brief file description
 *
 *  Created on: Mar 22, 2013
 *      Author: Mark Cutler
 *     Contact: markjcutler@gmail.com
 *
 */

#include "QuadDynamics.hpp"

namespace acl
{

QuadDynamics::QuadDynamics()
{

    // Initialize all variables
    simTime = 0.0;

    // Set default initial parameters

    // BQxx
    param.m = 0.416; // kg mass of BQ04
    param.Jx = 0.0042;
    param.Jy = 0.0042;
    param.Jz = 0.0082;
    param.l = 0.152;
    param.Kmotor = 0.01843;
    param.drag = 0.14;
    param.Kmx = param.Jx / (param.l * param.Kmotor * 2);
    param.Kmz = param.Jz / (param.l * 4 * param.drag);
    param.motor_scale = 4.7; // scaling from BQ04 measured values

    // Set default initial state
    state.x = state.y = state.z = state.u = state.v = state.w = 0.0;
    state.p = state.q = state.r = 0.0;
    state.Q = Quaternion();

    // construct a trivial random generator engine from a time-based seed:
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    generator.seed(seed);

    // Set up noise values
    noise.velocity_variance = 8;
    updateNoise();
}

QuadDynamics::~QuadDynamics()
{
    // TODO Auto-generated destructor stub
}

/**
 *
 * @return
 */
struct sQuadState QuadDynamics::getState(void)
{
    return this->state;
}

/**
 * Update the internal noise generators with the correct variances
 */
void QuadDynamics::updateNoise(void)
{
    velocity_noise = std::normal_distribution<double>(0.0,
            noise.velocity_variance);
}

/**
 *
 * @return
 */
struct sQuadParam QuadDynamics::getParams(void)
{
    return this->param;
}

/**
 *
 * @return
 */
struct sNoiseParam QuadDynamics::getNoise(void)
{
    return this->noise;
}

/**
 * Initialize state and reset time to zero
 * @param initState Initial state value
 */
void QuadDynamics::setInitialState(struct sQuadState initState)
{
    state = initState;
    simTime = 0;
}

/**
 * Set noise parameters for simulation
 * @param n
 */
void QuadDynamics::setNoiseStruct(struct sNoiseParam n)
{
    noise = n;
    updateNoise();
}

/**
 * Set quad parameters
 * @param p
 */
void QuadDynamics::setParamStruct(struct sQuadParam p)
{
    param = p;
}

/**
 * Set thrust and moment values
 */
void QuadDynamics::setThrustMoments(double F, double Mx, double My, double Mz)
{
    this->F = F;
    this->Mx = Mx;
    this->My = My;
    this->Mz = Mz;
}

/**
 * Integrate the system dynamics one time step
 * @param dt Time step
 */
void QuadDynamics::integrateStep(double dt)
{
    std::valarray<double> currentState(STATE_LENGTH);
    std::valarray<double> nextState(STATE_LENGTH);

    currentState[0] = state.x;
    currentState[1] = state.y;
    currentState[2] = state.z;
    currentState[3] = state.u;
    currentState[4] = state.v;
    currentState[5] = state.w;
    currentState[6] = state.Q.getW();
    currentState[7] = state.Q.getX();
    currentState[8] = state.Q.getY();
    currentState[9] = state.Q.getZ();
    currentState[10] = state.p;
    currentState[11] = state.q;
    currentState[12] = state.r;

    // run the RK4 integration step
    nextState = acl::rk4(simTime, currentState, dt,
            boost::bind(&QuadDynamics::dynamics, this, _1, _2));
    simTime += dt;

    state.x = nextState[0];
    state.y = nextState[1];
    state.z = nextState[2];
    state.u = nextState[3];
    state.v = nextState[4];
    state.w = nextState[5];
    state.Q.setW(nextState[6]);
    state.Q.setX(nextState[7]);
    state.Q.setY(nextState[8]);
    state.Q.setZ(nextState[9]);
    state.p = nextState[10];
    state.q = nextState[11];
    state.r = nextState[12];
}

/**
 * System dynamics
 * @param dt Time step
 * @param s State
 * @return Derivative of state
 */
std::valarray<double> QuadDynamics::dynamics(double dt, std::valarray<double> s)
{

    // double x  = s[0];
    // double y  = s[1];
    // double z  = s[2];
    double u = s[3];
    double v = s[4];
    double w = s[5];
    Quaternion Q(s[6], s[7], s[8], s[9]);
    double p = s[10];
    double q = s[11];
    double r = s[12];

    // System dynamics
    Quaternion posdot = Q * Quaternion(0, u, v, w) * Q.conj();
    double xdot = posdot.getX();
    double ydot = posdot.getY();
    double zdot = posdot.getZ();
    double udot = GRAVITY * 2.0 * (Q.getX() * Q.getZ() - Q.getY() * Q.getW())
            + r * v - q * w;
    double vdot = GRAVITY * 2.0 * (Q.getY() * Q.getZ() + Q.getX() * Q.getW())
            + p * w - r * u;
    double wdot = GRAVITY
            * (Q.getZ() * Q.getZ() + Q.getW() * Q.getW() - Q.getX() * Q.getX()
                    - Q.getY() * Q.getY()) - 1.0 / param.m * F + q * u - p * v;
    Quaternion Qdot = Quaternion(0.5) * Q * Quaternion(0, u, v, w);
    double pdot = 1.0 / param.Jx * (q * r + Mx);
    double qdot = 1.0 / param.Jy * (p * r + My);
    double rdot = 1.0 / param.Jz * (p * q + Mz);

    std::valarray<double> out(STATE_LENGTH);
    out[0] = xdot;
    out[1] = ydot;
    out[2] = zdot;
    out[3] = udot;
    out[4] = vdot;
    out[5] = wdot;
    out[6] = Qdot.getW();
    out[7] = Qdot.getX();
    out[8] = Qdot.getY();
    out[9] = Qdot.getZ();
    out[10] = pdot;
    out[11] = qdot;
    out[12] = rdot;

    return out;
}

} /* namespace acl */
