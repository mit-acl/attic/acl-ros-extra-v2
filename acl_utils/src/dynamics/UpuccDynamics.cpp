/*!
 * \file UpuccDynamics.cpp
 *
 * @todo Brief file description
 *
 *  Created on: Mar 22, 2013
 *      Author: Mark Cutler
 *     Contact: markjcutler@gmail.com
 *
 */

#include "UpuccDynamics.hpp"

namespace acl
{

UpuccDynamics::UpuccDynamics()
{

    // Initialize all variables
    simTime = 0.0;

    // Set default initial parameters
    param.Lw = 0.083;

    // Set default initial state and inputs
    state.x = state.y = state.psi = 0.0;
    vL = vR = 0.0;
}

UpuccDynamics::~UpuccDynamics()
{
    // TODO Auto-generated destructor stub
}

/**
 *
 * @return
 */
struct sUpuccState UpuccDynamics::getState(void)
{
    return this->state;
}

/**
 *
 * @return
 */
struct sUpuccParam UpuccDynamics::getParams(void)
{
    return this->param;
}

/**
 * Initialize state and reset time to zero
 * @param initState Initial state value
 */
void UpuccDynamics::setInitialState(struct sUpuccState initState)
{
    state = initState;
    simTime = 0;
}

/**
 * Set car parameters
 * @param p
 */
void UpuccDynamics::setParamStruct(struct sUpuccParam p)
{
    param = p;
}

/**
 * Sets left and right wheel velocities.
 * @param vL
 * @param vR
 */
void UpuccDynamics::setV(double vL, double vR)
{
    this->vL = vL;
    this->vR = vR;
}

/**
 * Integrate the system dynamics one time step
 * @param dt Time step
 */
void UpuccDynamics::integrateStep(double dt)
{
    std::valarray<double> currentState(STATE_LENGTH);
    std::valarray<double> nextState(STATE_LENGTH);

    currentState[0] = state.x;
    currentState[1] = state.y;
    currentState[2] = state.psi;

    // run the RK4 integration step
    nextState = acl::rk4(simTime, currentState, dt,
            boost::bind(&UpuccDynamics::dynamics, this, _1, _2));
    simTime += dt;

    state.x = nextState[0];
    state.y = nextState[1];
    state.psi = nextState[2];
}

/**
 * System dynamics
 * @param dt Time step
 * @param s State
 * @return Derivative of state
 */
std::valarray<double> UpuccDynamics::dynamics(double dt,
        std::valarray<double> s)
{

    // double x  = s[0];
    // double y  = s[1];
    double psi = s[2];

    double v = 0.5 * (vR + vL);
    double dv = vR - vL;

    // System dynamics
    double xdot = v * cos(psi);
    double ydot = v * sin(psi);
    double psidot = dv / param.Lw;

    std::valarray<double> out(STATE_LENGTH);
    out[0] = xdot;
    out[1] = ydot;
    out[2] = psidot;

    return out;
}

} /* namespace acl */
