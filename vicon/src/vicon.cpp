/*!
 * \file vicon.cpp
 *
 * Pulls data from vicon, filters it, and broadcasts it on the network
 *
 * Created on: Oct 16, 2013
 * 	   Author: Mark Cutler
 *      Email: markjcutler@gmail.com
 */

#include "vicon.h"

 using namespace ViconDataStreamSDK::CPP;

 Vicon::Vicon()
 {
 	t_prev = 0.0;
	// shouldBroadcast = true;  // this is no longer applicable -- TODO: take out
	dt_filt = DESIRED_DT; //Initialize 
	initClient();
}

Vicon::~Vicon()
{
	// Disconnect and dispose
	MyClient.Disconnect();
}

void Vicon::updateBodyMap()
{
	//## Variables for tracked bodies
	numCurrentBodies = MyClient.GetSubjectCount().SubjectCount;
	std::vector<std::string> subjNameVec;
	// ROS_INFO_STREAM_THROTTLE(5,"Subject Names:"):
	for (int index = 0; index < numCurrentBodies; index++){
		subjNameVec.push_back(MyClient.GetSubjectName(index).SubjectName);
	}


	// Add new Body to the BodyMap 
	std::vector<std::string>::iterator subjNameIter;
	for (subjNameIter = subjNameVec.begin(); subjNameIter != subjNameVec.end(); subjNameIter++ ){
		if (BodyMap.find(*subjNameIter)==BodyMap.end()){
			// Subject not yet in the BodyMap, initialize a Body and set its name
			BodyMap[*subjNameIter].resetStates();
			BodyMap[*subjNameIter].setName(*subjNameIter);
			ROS_INFO_STREAM("Add to BodyMap: " << *subjNameIter);
		}
	}

	// Remove Body that no longer has a subject
	std::map<std::string,Body>::iterator mapIter = BodyMap.begin();
	while (mapIter != BodyMap.end()) {
		if(std::find(subjNameVec.begin(),subjNameVec.end(),mapIter->first) == subjNameVec.end()){
			// Key not in subjNameVec. Erase and then advance.
			ROS_INFO_STREAM("Removing from BodyMap: " << mapIter->first);
			BodyMap.erase(mapIter++);
		}
		else{
			// Key is in subjNameVec. Advance.
			++mapIter;
		}
	}
}

bool Vicon::runFilter()
{
	// Get a frame
	//Waiting for a new frame...
	while (MyClient.GetFrame().Result != Result::Success){}

	// Handle Add/Remove Bodies
	updateBodyMap();
	
	dt_filt = calcDT();
	// ROS_INFO_STREAM("dt_filt is:" << dt_filt);
	
	std::map<std::string,Body>::iterator mapIter;
	for (std::map<std::string,Body>::iterator mapIter = BodyMap.begin();mapIter != BodyMap.end(); mapIter++)
	{
		//************** Get measurement Data from Vicon ************/
		bool occluded;
		unsigned int markers_count;
		unsigned int max_num_of_markers;
		// Get observation from Vicon
		geometry_msgs::Pose measurement = getViconData(mapIter->first,
			occluded,markers_count, max_num_of_markers);
		//******************  FILTERING  *********************/
		mapIter->second.update(dt_filt, measurement, occluded, markers_count, max_num_of_markers);
		mapIter->second.broadcastData();
	}
	return true;
}

void Vicon::screenPrint()
{
	ROS_INFO_STREAM_THROTTLE(5, "Current Vicon rate is: " << 1.0/dt_filt <<" Hz");
	// ROS_INFO_STREAM("Current Vicon rate is: " << 1.0/dt_filt <<" Hz");
}

// this function reads the dt values from the vicon time stamps
double Vicon::calcDT()
{

    // get dt from Vicon

	// expected dt
	//Output_GetFrameRate fr = MyClient.GetFrameRate();
//	if (fr.Result==Result::Success){
//		// expected frame rate
//		double frame_rate = fr.FrameRateHz;
//		dt = 1.0/frame_rate;
//	}

	// measured dt -- WARNING - only tested with PAL, but should work for other frame rates
	Output_GetTimecode time = MyClient.GetTimecode();
	if (time.Result==Result::Success){

		// expected frame rate
		//double frame_rate = fr.FrameRateHz;
		//dt = 1.0/frame_rate;

		// check if this is the frame that expected, or if we have been dropping frames
		double current_time = time.Seconds + 60*time.Minutes + 3600*time.Hours;
		double frames = time.Frames + (double) time.SubFrame/time.SubFramesPerFrame;

		switch(time.Standard)
		{
			case TimecodeStandard::PAL:
				current_time += frames/25.0;
				break;
			case TimecodeStandard::NTSC:
				current_time += frames/29.97;
				break;
			case TimecodeStandard::NTSCDrop:
			    //current_time += frames/29.97;
				ROS_ERROR_STREAM_THROTTLE(0.5, "Wrong Timecode standard!");
				break;
			case TimecodeStandard::Film:
				current_time += frames/24.0;
				break;
			default:
				break;
		}

		if (t_prev == 0.0){
			// Fisrt time
			t_prev = current_time;
			return 0.01;
		}
		else{
			double dt = current_time - t_prev;
			t_prev = current_time;
			dt = acl::saturate(dt, 10.0, 0.0001); // For handling vicon rebooting. (Was causing large dt and nan in attitude.)
			return dt;			
		}

		//	    std::cout << "FrameRate: " << frame_rate << std::endl;
		//		std::cout << "Hours: " << time.Hours << " Min: " << time.Minutes << " Sec: " << time.Seconds <<
		//		" Frame: " << time.Frames << " SubFrames: " << time.SubFrame << " SubFrameperSec: " <<
		//		time.SubFramesPerFrame << " Timecodestd: " << time.Standard << " None: " << TimecodeStandard::None << std::endl;
		// std::cout << "Seconds: " << current_time << ", Frames: " << frames << ", dt_meas: " << dt << std::endl;

		

	}

	ROS_ERROR_STREAM("We shouldn't reach here, but if we do, it's bad.");
	return -1;
}

void Vicon::initClient()
{
	// Get host ip
	std::string host;
	if(ros::param::has("~host")){
		ros::param::getCached("~host",host);
	}
	else{
		host = "192.168.0.9:801";
		ros::param::set("~host",host);
	}

	// Connect to a server
	std::cout << "Connecting..." << std::flush;
	while (!MyClient.IsConnected().Connected)
	{
		// Direct connection
		MyClient.Connect(host);
		std::cout << ".";

	}
	std::cout << std::endl;

	// Enable some different data types
	MyClient.EnableSegmentData();
	MyClient.EnableMarkerData();
	MyClient.EnableUnlabeledMarkerData();
	MyClient.EnableDeviceData();

	// Set the streaming mode
	//MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPull );
	// Output_SetStreamMode _Output_SetStreamMode = MyClient.SetStreamMode( ViconDataStreamSDK::CPP::StreamMode::ClientPullPreFetch );
	MyClient.SetStreamMode(ViconDataStreamSDK::CPP::StreamMode::ServerPush); // this is the lowest latency version

	// Set the global up axis
	MyClient.SetAxisMapping(Direction::Forward, Direction::Left, Direction::Up); // Z-up

	// Skip some frames to avoid the "leftover" subject names from last conneciton.
	// This is perhaps due to the ServerPush mode.
	for (int i = 0; i < 25; i++){	
		while (MyClient.GetFrame().Result != Result::Success){}
	}
	ROS_INFO_STREAM("Vicon filter initialized.");
	// for (int index = 0; index < MyClient.GetSubjectCount().SubjectCount; index++){
	// 	ROS_INFO_STREAM(MyClient.GetSubjectName(index).SubjectName);
	// }
}

geometry_msgs::Pose Vicon::getViconData(std::string name, bool &occluded){
	unsigned int markers_count;
	unsigned int max_num_of_markers;
	return getViconData(name, occluded, markers_count, max_num_of_markers);
}

geometry_msgs::Pose Vicon::getViconData(std::string name, bool &occluded, unsigned int &markers_count, unsigned int &max_num_of_markers)
{
	geometry_msgs::Pose measurement;
	//Need these as a reference
	// We only care about segment #0
	unsigned int SegmentIndex = 0;
	std::string SegmentName = MyClient.GetSegmentName(name, SegmentIndex).SegmentName;

	//However we're only going to take the first segment of the body
	Output_GetSegmentGlobalTranslation _Output_GetSegmentGlobalTranslation =
	MyClient.GetSegmentGlobalTranslation(name, SegmentName);
	measurement.position.x = _Output_GetSegmentGlobalTranslation.Translation[0]/1000.0;
	measurement.position.y = _Output_GetSegmentGlobalTranslation.Translation[1]/1000.0;
	measurement.position.z = _Output_GetSegmentGlobalTranslation.Translation[2]/1000.0;
	occluded = _Output_GetSegmentGlobalTranslation.Occluded;

	Output_GetSegmentGlobalRotationQuaternion _Output_GetSegmentGlobalRotationQuaternion =
	MyClient.GetSegmentGlobalRotationQuaternion(name, SegmentName);
	measurement.orientation.x = _Output_GetSegmentGlobalRotationQuaternion.Rotation[0];
	measurement.orientation.y =	_Output_GetSegmentGlobalRotationQuaternion.Rotation[1];
	measurement.orientation.z =	_Output_GetSegmentGlobalRotationQuaternion.Rotation[2];
	measurement.orientation.w =	_Output_GetSegmentGlobalRotationQuaternion.Rotation[3];

	// Get total number of markers
	Output_GetMarkerCount _Output_GetMarkerCount;
	_Output_GetMarkerCount = MyClient.GetMarkerCount(SegmentName);
	max_num_of_markers = _Output_GetMarkerCount.MarkerCount;

	// Go through markers and check for occlusion on each
	markers_count = 0;
	for (int i = 0; i < max_num_of_markers; i++){
		// std::cout << "HERE!" << std::endl;
		Output_GetMarkerName _OutputGMN = MyClient.GetMarkerName(SegmentName, i);
		// std::cout << "Marker Name:" << _OutputGMN.MarkerName << std::endl;
		Output_GetMarkerGlobalTranslation _OutputGMGT =
			MyClient.GetMarkerGlobalTranslation(SegmentName,_OutputGMN.MarkerName);
		// TODO:Check Result.Success
		if (not _OutputGMGT.Occluded){
			// Count unoccluded markers
			markers_count++;
		}
	}	
	return measurement;
}



void Vicon::printLatency()
{
	// Get the latency
	std::cout << "Latency: " << MyClient.GetLatencyTotal().Total << "s"
	<< std::endl;

	for (unsigned int LatencySampleIndex = 0;
		LatencySampleIndex < MyClient.GetLatencySampleCount().Count;
		++LatencySampleIndex)
	{
		std::string SampleName = MyClient.GetLatencySampleName(
			LatencySampleIndex).Name;
		double SampleValue = MyClient.GetLatencySampleValue(SampleName).Value;

		std::cout << "  " << SampleName << " " << SampleValue << "s"
		<< std::endl;
	}
	std::cout << std::endl;
}
