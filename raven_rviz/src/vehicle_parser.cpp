/*
 * vehicle_parser.cpp
 *
 *  Created on: Sep 25, 2013
 *  Author: Mark -- adopted from code from Bobby
 *
 *
 *  Queries the master list of topics and selects those of the form
 *  /<vehType>/pose, where <vehType> is of the form AA11(s) (two letters then
 *  two numbers with optional 's' at the end to denote simulated vehicle).
 *
 *  Publishes a list of the all the vehicles that meet this critereon (without
 *  checking if they have current data being published or not.
 *
 */

// Global includes
#include <ros/ros.h>
#include <ros/master.h>
#include <boost/algorithm/string.hpp>

// Local includes
#include "acl_msgs/VehicleList.h"

enum {
    LOOP_RATE = 1 // Hz
};

// Check if a string contains only positive integers
bool is_number(const std::string& s)
{
    for (int i = 0; i < s.length(); i++) // for each char in string,
        if (not(s[i] >= '0' and s[i] <= '9'))
            return false;
    return true;
}

// Check if a string contains only letters
bool is_letter(const std::string& s)
{
    for (int i = 0; i < s.length(); i++) // for each char in string,
        if (not((s[i] >= 'A' and s[i] <= 'Z') or (s[i] >= 'a' and s[i] <= 'z')))
            return false;
    return true;
}

/** Determine if the string <topicName> corresponds to a valid vehicle pose
 * topic,
 *  and assign the string agentName for later use.
 */
bool checkVehPoseTopic(std::string topicName, std::string& agentName)
{
    std::vector<std::string> tmp;
    boost::split(tmp, topicName, boost::is_any_of("/"),
                 boost::token_compress_on); // of the form: /uP01/pose

    if (tmp.size() != 3) { // cannot be a vehicle topic
        ROS_DEBUG_STREAM("Not a sufficiently long topic\n");
        return false;
    }
    if (!tmp.at(2).compare("vicon")) { // This is a pose topic
        ROS_DEBUG_STREAM("This is a vicon topic!\n");
        std::string agentType = tmp.at(1).substr(0, 2); // first two characters
        std::string agentNumber = tmp.at(1).substr(2, 2); // next two characters
        // check that agent type is two letters and that the agent number is two
        // numbers
        if (is_letter(agentType) and is_number(agentNumber)) {
            ROS_DEBUG_STREAM("Valid topic!\n");
            ROS_DEBUG_STREAM("Agent Type is: " << agentType << std::endl);
            ROS_DEBUG_STREAM("Agent Number is: " << agentNumber << std::endl);
            agentName = tmp.at(1);
            return true;
        }
    }
    ROS_DEBUG_STREAM("Invalid topic\n");
    return false;
}

// Get a list of the current topics and store them in a vehiclelist message
acl_msgs::VehicleList updateCurrentTopics(ros::master::V_TopicInfo& allTopics)
{

    ros::master::getTopics(allTopics);
    acl_msgs::VehicleList v;
    for (unsigned int i = 0; i < allTopics.size(); i++) {
        std::string topicName = allTopics[i].name; // /uP04/pose
        std::string agentName;
        if (checkVehPoseTopic(
                topicName, agentName)) { // This topic is a vehicle pose topic
            v.vehicle_names.push_back(agentName);
        }
    }
    return v;
}

int main(int argc, char** argv)
{

    ros::init(argc, argv, "vehicle_parser");

    ros::NodeHandle node;

    ros::Rate loop_rate(LOOP_RATE); // Hz
    ros::Publisher vpub
        = node.advertise<acl_msgs::VehicleList>("current_vehicles", 1);

    ros::master::V_TopicInfo allTopics;

    while (ros::ok()) {
        // get list of all valid topics and publish to network
        acl_msgs::VehicleList v = updateCurrentTopics(allTopics);
        vpub.publish(v);
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
