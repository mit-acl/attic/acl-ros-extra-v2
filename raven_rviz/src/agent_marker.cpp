/*
 * agent_marker.cpp
 *
 *  Created on: Sep 26, 2013
 *      Author: Mark Cutler
 *              Email:  markjcutler@gmail.com
 */

#include "agent_marker.h"

AgentMarker::AgentMarker(std::string name_in, int id)
{
        // set name
        name = name_in;

        // Set default marker values
        scale.x = scale.y = scale.z = 1.0;
        color.r = color.a = 1.0;
        color.g = color.b = 0.0;
        mesh_resource = "gpucc.STL";
        use_embedded_materials = false;
        roll_offset = pitch_offset = yaw_offset = 0.0;
        x_offset = y_offset = z_offset = 0.0;

        // override any of these default values with ones from the parm server
        getParamsFromServer(name.substr(0,2)); // use first two letters as key

        // check for if this vehicle is simulated or not
        if (name.size() > 4 and (name[4] == 's' or name[4] == 'S'))
        {
                // this is a simulated vehicle -- decrease alpha to indicate this
                ROS_INFO_STREAM("This is a simulated vehicle");
                color.a = 0.4;
                color.r = color.g = color.b = 1.0;
        }

        markerID = id;

        // subscribe to pose message -- Note, this should be ok to do since
        // this class should only get initialized if there is a valid pose
        // topic for this vehicle to subscribe to
        ros::NodeHandle nh;
        sub_pose = nh.subscribe(name + "/vicon", 1, &AgentMarker::viconCallback,
                        this);
        initMarker();
        ROS_INFO_STREAM("Initializing agent marker for " << name);
}

AgentMarker::~AgentMarker()
{
        // TODO Auto-generated destructor stub
}

visualization_msgs::Marker AgentMarker::getMarker()
{
        return markerVehicle;
}

visualization_msgs::Marker AgentMarker::getTextMarker()
{
        return markerText;
}


// We should broadcast this marker if we have received pose data in the
// last MAXTIMEOUT seconds
bool AgentMarker::shouldBroadcast()
{
        double elapsedTime = ros::Time().now().toSec()
                        - markerVehicle.header.stamp.toSec();
        ROS_DEBUG_STREAM("elapsed time: " << elapsedTime);
        if (elapsedTime < MAXTIMEOUT)
        {
                return true;
        }

        return false;
}

void AgentMarker::initMarker()
{
        // Constant settings that are the same for all vehicles
        markerVehicle.header.frame_id = "vicon";
        markerVehicle.header.stamp = ros::Time::now();
        markerVehicle.ns = "RAVEN_vehicles";
        markerVehicle.id = markerID;
        markerVehicle.type = visualization_msgs::Marker::MESH_RESOURCE;
        markerVehicle.action = visualization_msgs::Marker::ADD;
        markerVehicle.lifetime = ros::Duration(3); // 3 second lifetime

        // Agent specific settings
        std::string stlPathFile = "package://raven_rviz/vehicle_models/";
        markerVehicle.scale = scale;
        markerVehicle.color = color;
        markerVehicle.mesh_resource = stlPathFile + mesh_resource;
        markerVehicle.mesh_use_embedded_materials = use_embedded_materials;

        // text
        markerText.header = markerVehicle.header;
        markerText.ns = "RAVEN_vehicles_text";
        markerText.id = markerID;
        markerText.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
        markerText.action = visualization_msgs::Marker::ADD;
        markerText.lifetime = ros::Duration(3);
        markerText.scale.z = 0.05; // height of an uppercase "A"
        markerText.color.r = markerText.color.g = markerText.color.b = 1.0;
        markerText.color.a = 1.0;
        markerText.text = name;
}

// Get pose data for this marker
void AgentMarker::viconCallback(const acl_msgs::ViconState &msg)
{
        markerVehicle.header.stamp = ros::Time::now(); //msg.header.stamp;
        markerVehicle.pose = msg.pose;
        markerText.header.stamp = markerVehicle.header.stamp;
        markerText.pose = msg.pose;
        markerText.pose.position.z += 0.2;
    double yaw_temp = tf::getYaw(msg.pose.orientation);
        markerVehicle.pose.position.x = msg.pose.position.x + cos(yaw_temp) * x_offset - sin(yaw_temp) * y_offset;
        markerVehicle.pose.position.y = msg.pose.position.y + sin(yaw_temp) * x_offset + cos(yaw_temp) * y_offset;
        markerVehicle.pose.position.z = msg.pose.position.z + z_offset;

        // Rotate the marker by a fixed offest if your model's axes don't align
        // with RAVEN's axes
        tf::Quaternion tmprot; tmprot.setEuler(0.0,0.0,yaw_offset);
        tf::Quaternion tmpMarker;  tf::quaternionMsgToTF(markerVehicle.pose.orientation,tmpMarker);
        tmpMarker = tmpMarker*tmprot;
        tmprot.setEuler(0.0,pitch_offset,0.0);
        tmpMarker = tmpMarker*tmprot;
        tmprot.setEuler(roll_offset,0.0,0.0);
        tmpMarker = tmpMarker*tmprot;
        tf::quaternionTFToMsg(tmpMarker,markerVehicle.pose.orientation);
}

// get parameters from the param server for displaying this vehicle
void AgentMarker::getParamsFromServer(std::string nameType)
{
        param(nameType + "/scale/x", scale.x);
        param(nameType + "/scale/y", scale.y);
        param(nameType + "/scale/z", scale.z);
        param(nameType + "/color/a", color.a);
        param(nameType + "/color/r", color.r);
        param(nameType + "/color/g", color.g);
        param(nameType + "/color/b", color.b);
        param(nameType + "/mesh_resource", mesh_resource);
        param(nameType + "/use_embedded_materials", use_embedded_materials);
        param(nameType + "/offset/roll", roll_offset);
        param(nameType + "/offset/pitch", pitch_offset);
        param(nameType + "/offset/yaw", yaw_offset);
        param(nameType + "/offset/x", x_offset);
        param(nameType + "/offset/y", y_offset);
        param(nameType + "/offset/z", z_offset);

}

void AgentMarker::param(std::string n, float &out)
{
        if (ros::param::get(n, out))
                ROS_DEBUG_STREAM("Got param: '" << n << "', " << out);
        else
                ROS_WARN_STREAM("Using default value for param: '" << n << "' = " << out);
}

void AgentMarker::param(std::string n, double &out)
{
        if (ros::param::get(n, out))
                ROS_DEBUG_STREAM("Got param: '" << n << "', " << out);
        else
                ROS_WARN_STREAM("Using default value for param: '" << n << "' = " << out);
}

void AgentMarker::param(std::string n, bool &out)
{
        if (ros::param::get(n, out))
                ROS_DEBUG_STREAM("Got param: '" << n << "', " << out);
        else
                ROS_WARN_STREAM("Using default value for param: '" << n << "' = " << out);
}

void AgentMarker::param(std::string n, std::string &out)
{
        if (ros::param::get(n, out))
                ROS_DEBUG_STREAM("Got param: '" << n << "', " << out);
        else
                ROS_WARN_STREAM("Using default value for param: '" << n << "' = " << out);
}
