#!/usr/bin/env python
import rospy
import copy
import collections
from geometry_msgs.msg import Point, Pose#, PointStamped
from acl_msgs.msg import VehicleList, QuadGoal, QuadWaypoint, QuadWaypointError, ViconState, QuadWaypointQueueComplete, BoolStamped
from waypoint_manager.srv import SetNewWaypoint
from std_srvs.srv import Empty, EmptyResponse
from sensor_msgs.msg import Joy
import numpy as np
import tf

class Quad(object):
    def __init__(self, veh_name):
        self.veh_name = veh_name
        self.waypoint_queue = collections.deque()
        self.skip_prev_wpt = False
        self.last_wpt_timestamp = -999
        self.first_wpt_sent = False

        self.wpt_tol_pos = 0.2 
        self.wpt_tol_yaw = float(15)/180*np.pi # convert degree error to rads
        self.z_nominal = 0.75 #TODO param
        self.has_initiated_takeoff = False
        self.has_finished_takeoff = False

        self.pub_waypoint = rospy.Publisher(self.veh_name + "/global_goal", QuadWaypoint, queue_size=10, latch=True)
        self.pub_joy_cmd = rospy.Publisher("/joy", Joy, queue_size=100, latch=True)
        
        # Waypoint servers
        self.srv_go_to_now = rospy.Service(self.veh_name + "/wpt_go_to_now", SetNewWaypoint, self.cbSrvGoToNow)
        self.srv_add_waypoint = rospy.Service(self.veh_name + "/wpt_add", SetNewWaypoint, self.cbSrvAddWaypoint)
        self.srv_land_at = rospy.Service(self.veh_name + "/wpt_land_at", SetNewWaypoint, self.cbSrvLandAt)
        self.srv_land_now = rospy.Service(self.veh_name + "/wpt_land_now", Empty, self.cbSrvLandNow)
        self.srv_takeoff = rospy.Service(self.veh_name + "/wpt_takeoff", Empty, self.cbSrvTakeoff)
        # self.srv_pick_up_pkg_at = rospy.Service(self.veh_name + "/wpt_pick_up_pkg_at", SetNewWaypoint, self.cbSrvPickUpPkgAt)

    def pushWaypoint(self,quadwaypoint):
        rospy.loginfo("[wpt_manager][pushWaypoint][%s] Added new wpt %s to queue." %(self.veh_name, self.getWptString(quadwaypoint.point)))
        wpt = QuadWaypoint()
        wpt.header.stamp = rospy.Time.now()
        wpt.header.frame_id = 'vicon'
        wpt.point.x = quadwaypoint.point.x
        wpt.point.y = quadwaypoint.point.y
        wpt.point.z = quadwaypoint.point.z
        wpt.heading = quadwaypoint.heading # in rads, in building 41 coordinate system -- TODO 2017 get from mission manager
        self.waypoint_queue.appendleft(wpt)

    def popWaypoint(self):
        return self.waypoint_queue.pop()

    def popAndPubNextWaypoint(self):
        is_wpt_queue_empty = True
        if (len(self.waypoint_queue)>0):
            self.latest_wpt_sent = self.popWaypoint()
            self.last_wpt_timestamp = self.latest_wpt_sent.header.stamp
            self.pub_waypoint.publish(self.latest_wpt_sent)
            self.first_wpt_sent = True
            is_wpt_queue_empty = False
        return is_wpt_queue_empty

    def getYawFromPose(self, pose):
        quat = (pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quat) # Roll, pitch, yaw
        return euler[2]

    def wrapAngle(self, rads):
        if rads > np.pi:
            return rads - 2.*np.pi
        elif rads < -np.pi:
            return rads + 2.*np.pi
        else:
            return rads

    def getWaypointErr(self):
        if self.first_wpt_sent:
            vicon_state = self.getSingleViconMsg()
            pos_err = (vicon_state.pose.position.x - self.latest_wpt_sent.point.x)**2 + (vicon_state.pose.position.y - self.latest_wpt_sent.point.y)**2 + (vicon_state.pose.position.z - self.latest_wpt_sent.point.z)**2
            yaw_err = abs(self.wrapAngle(self.getYawFromPose(vicon_state.pose) - self.latest_wpt_sent.heading))
            return pos_err, yaw_err
        else:
            return 9999, 9999

    def getWptString(self, point):
        if point is not None:
            return '[%f,%f,%f]'%(point.x,  point.y,  point.z)
        else:
            return '[None wpt (indicates fresh queue)]'


    def shouldGoToNextWpt(self):
        # Case: being forced to skip wpts in queue, so just send the next wpt
        if self.skip_prev_wpt:
            self.skip_prev_wpt = False
            return True
        
        # Case: waypoint reached
        wpt_pos_err, wpt_yaw_err = self.getWaypointErr()
        if not self.first_wpt_sent or (wpt_pos_err < self.wpt_tol_pos and wpt_yaw_err < self.wpt_tol_yaw):
            # if self.first_wpt_sent:
            #     print 'completed with following stats:'
            #     print 'wpt_pos_err', wpt_pos_err
            #     print 'wpt_yaw_err', wpt_yaw_err
            #     print '---------'
            return True
        
        return False

    def getSingleViconMsg(self):
        return rospy.wait_for_message(self.veh_name + "/vicon", ViconState)

    def pushSafeTakeoffWpt(self):
        # If quad has not taken off, always send a one-time takeoff wpt before any other wpts sent
        # This ensures the quad doesn't immediately try to go to a far wpt from the ground
        if not self.has_initiated_takeoff:
            vicon_state = self.getSingleViconMsg()
            takeoffWpt = QuadWaypoint()
            takeoffWpt.point.x = vicon_state.pose.position.x
            takeoffWpt.point.y = vicon_state.pose.position.y
            takeoffWpt.point.z = self.z_nominal
            takeoffWpt.heading = self.getYawFromPose(vicon_state.pose)

            self.pushWaypoint(takeoffWpt)
            self.has_initiated_takeoff = True

    def cbSrvGoToNow(self,req):
        rospy.loginfo("[wpt_manager][cbSrvGoToNow][%s] cbSrvGoToNow called."%(self.veh_name)) # Added new wpt %s to queue, and going to it immediately." %(self.veh_name, self.getWptString(req.pose.position)))
        # Ensure waypoint_manager main loop doesn't wait for previous waypoint to be satisfied before pushing this one
        self.waypoint_queue.clear()
        self.pushSafeTakeoffWpt()
        self.pushWaypoint(req.quadwaypoint)

        # Order matters, due to threading (services and mainLoop), so leave self.skip_prev_wpt down here
        self.skip_prev_wpt = True

        return True

    def cbSrvTakeoff(self,req):
        rospy.loginfo("[wpt_manager][cbSrvTakeoff][%s] taking off." %(self.veh_name))
        self.has_finished_takeoff = False
        
        # msg_trigger_ready = BoolStamped()
        # msg_trigger_ready.header.stamp = rospy.Time.now()
        # msg_trigger_ready.data = True
        # self.pub_trigger_ready.publish(msg_trigger_ready)

        # TODO 2017 - this should not be a joystick msg, should have correct triggering in safety_fsm_tip instead
        self.sendTakeoffJoyCmd()

        self.waypoint_queue.clear()
        self.pushSafeTakeoffWpt()

        # Order matters, due to threading (services and mainLoop), so leave self.skip_prev_wpt down here
        self.skip_prev_wpt = False
        return EmptyResponse()

    def cbSrvAddWaypoint(self,req):
        rospy.loginfo("[wpt_manager][cbSrvAddWaypoint][%s] cbSrvAddWaypoint called."%(self.veh_name)) #Added new wpt %s to queue." %(self.veh_name, self.getWptString(req.quadwaypoint.position)))
        self.pushSafeTakeoffWpt()
        self.pushWaypoint(req.quadwaypoint)
        return True

    # def cbSrvPickUpPkgAt(self,req):
    #     rospy.loginfo("[wpt_manager][cbSrvPickUpPkgAt][%s] Added new package pickup wpt to queue." %(self.veh_name))
        
    #     # Safe takeoff
    #     self.pushSafeTakeoffWpt()

    #     # Nominal z
    #     req.pose.position.z = 1#self.z_nominal
    #     self.pushWaypoint(req.pose)

    #     # Pickup z
    #     req.pose.position.z = 0.7
    #     self.pushWaypoint(req.pose)

    #     # Nominal z
    #     # req.pose.position.z = 1#self.z_nominal
    #     # self.pushWaypoint(req.pose, QuadWaypoint.MODE_TAKEOFF)
    #     return True

    def resetVarsAfterLand(self):
        self.first_wpt_sent = False
        self.has_initiated_takeoff = False
        self.has_finished_takeoff = False
        # Usually on landing, wpt.z goes into ground, so wpt_error will never be zero
        # This flag ensures mainLoop does not perpetually listen for wpt_error=0 after landing
        self.skip_prev_wpt = True

    def sendTakeoffJoyCmd(self):
        # TODO 2017 - this should not be a joystick msg, should have correct triggering in safety_fsm_tip instead
        joyMsg = Joy()
        joyMsg.header.stamp = rospy.Time.now()
        joyMsg.buttons = [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0] # Press the A button
        self.pub_joy_cmd.publish(joyMsg)

    def sendGoJoyCmd(self):
        # TODO 2017 - this should not be a joystick msg, should have correct triggering in safety_fsm_tip instead
        joyMsg = Joy()
        joyMsg.header.stamp = rospy.Time.now()
        joyMsg.buttons = [0,0,0,0,0,0,0,1,0,0,0,0,0,0,0] # Press the Start button
        self.pub_joy_cmd.publish(joyMsg)

    def sendLandJoyCmd(self):
        # TODO 2017 - this should not be a joystick msg, should have correct triggering in safety_fsm_tip instead
        joyMsg = Joy()
        joyMsg.header.stamp = rospy.Time.now()
        joyMsg.buttons = [0,0,1,0,0,0,0,0,0,0,0,0,0,0,0] # Press the X button
        self.pub_joy_cmd.publish(joyMsg)
        print 'sent joy land'

    def cbSrvLandAt(self,req):
        rospy.loginfo("[wpt_manager][cbSrvLandAt][%s] Added new landing wpt to queue." %(self.veh_name))
        req.quadwaypoint.point.z = self.z_nominal
        self.pushWaypoint(req.quadwaypoint)
        # TODO 2017 this basically skips going to the above waypoint if it hasnt been completed yet
        self.sendLandJoyCmd()#

        self.resetVarsAfterLand()
        return True

    def cbSrvLandNow(self,req):
        rospy.loginfo("[%s][cbSrvLandNow] Added new landing wpt to queue." %(self.veh_name))
        self.waypoint_queue.clear()

        # vicon_state = self.getSingleViconMsg()
        self.sendLandJoyCmd()

        # Order matters, due to threading (services and mainLoop), so leave self.skip_prev_wpt down here
        self.skip_prev_wpt = True
        self.resetVarsAfterLand()
        return EmptyResponse()

    def unregisterPubsAndServs(self):
        self.pub_waypoint.unregister()
        self.srv_go_to_now.shutdown()
        self.srv_add_waypoint.shutdown()
        # self.srv_pick_up_pkg_at.shutdown()
        self.srv_land_at.shutdown()
        self.srv_land_now.shutdown()
        self.srv_takeoff.shutdown()

class WaypointManager(object):
    def __init__(self):
        self.node_name = rospy.get_name()

        self.quad_dict = dict()
        self.sub_veh_list = rospy.Subscriber("~vehicle_list", VehicleList, self.cbVehList, queue_size=1)
        self.pub_wpt_queue_complete = rospy.Publisher("~wpt_queue_complete", QuadWaypointQueueComplete, queue_size=100)

        # Waypoint manager main loop
        self.managerTimer = rospy.Timer(rospy.Duration.from_sec(0.05), self.managerLoop)

    def pubWptQueueComplete(self, veh_name):
        msg = QuadWaypointQueueComplete()
        msg.header.stamp = rospy.Time.now()
        msg.veh_name = veh_name
        msg.is_queue_complete = True
        self.pub_wpt_queue_complete.publish(msg)

    def managerLoop(self,event):
        for veh_name in self.quad_dict.keys():
            cur_quad = self.quad_dict[veh_name]
            if (cur_quad.shouldGoToNextWpt()):
                # Save the latest wpt for logging purposes
                if cur_quad.first_wpt_sent:
                    wpt_just_finished = cur_quad.latest_wpt_sent.point
                else:
                    wpt_just_finished = None

                is_wpt_queue_empty = cur_quad.popAndPubNextWaypoint()
                if not is_wpt_queue_empty:
                    rospy.loginfo("[wpt_manager][managerLoop][%s] Successfully finished wpt %s, going to next wpt." %(cur_quad.veh_name, cur_quad.getWptString(wpt_just_finished)))
                else:
                    # Publish msg indicating wpt queue is complete for cur_quad, so external Mission Managers can proceed with their mission
                    if cur_quad.first_wpt_sent:
                        cur_quad.first_wpt_sent = False
                        rospy.loginfo("[wpt_manager][managerLoop][%s] Successfully finished wpt %s, and wpt queue is now empty." %(cur_quad.veh_name, cur_quad.getWptString(wpt_just_finished)))
                        self.pubWptQueueComplete(cur_quad.veh_name)
                    

    def cbVehList(self,veh_list_msg):
        # Add vehicle
        for veh_name in veh_list_msg.vehicle_names:
            if veh_name not in self.quad_dict.keys():
                self.addVehicle(veh_name)
                print '[wpt_manager][cbVehList] Added', veh_name

        # Remove vehicle
        for veh_name in self.quad_dict.keys():
            if veh_name not in veh_list_msg.vehicle_names:
                self.removeVehicle(veh_name)
                print '[wpt_manager][cbVehList] Removed', veh_name

    def addVehicle(self,veh_name):
        self.quad_dict[veh_name] = Quad(veh_name)

    def removeVehicle(self,veh_name):
        self.quad_dict[veh_name].unregisterPubsAndServs()
        self.quad_dict.pop(veh_name)

if __name__== "__main__":
    rospy.init_node("waypoint_manager_server")
    node = WaypointManager()
    rospy.spin()