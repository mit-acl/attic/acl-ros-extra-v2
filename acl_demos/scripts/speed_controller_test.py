#!/usr/bin/env python
# cartesian joystick waypoint control for quadrotor
import roslib
import rospy
import copy
import math
import numpy as np
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped, Pose, TwistStamped, Twist

# local imports
from acl_msgs.msg import ViconState
from acl_msgs.msg import QuadGoal
# from acl_msgs.msg import JoyDef
import utils

NOT_FLYING = 0
FLYING = 2
LANDING = 3

TAKEOFF = 1
DISABLE = 2
RESET_INTEGRATORS = 4
ATTITUDE = 5

LEFT_X = 0
LEFT_Y = 1
RIGHT_X = 3
RIGHT_Y = 4

A = 0
B = 1
X = 2
Y = 3
RB = 5
BACK = 6
START = 7
CENTER = 8

CONTROL_DT   = 0.01
MAX_ACCEL_XY = 2.0
MAX_ACCEL_Z  = 0.8

# ROOM Bounds
XMIN = -1.5
XMAX = 1.5
YMIN = -5.0
YMAX = 1.0
ZMIN = 0.5
ZMAX = 2.0

STEP 		= 0
RAMP 		= 1
SMOOTH 		= 2
SMOOTH_FF	= 3

READY       = 1
START    	= 2
CRUISE      = 3
HOVER       = 4
STOP        = 5
ESTOP       = 6


class speedTest:

	def __init__(self):

		# 0 for step, 1 for ramp, 2 for smooth, 3 for smooth + FF
		self.test = SMOOTH_FF

		self.status = NOT_FLYING
		self.transmitting = True
		self.wpType = DISABLE

		self.goal = QuadGoal()
		self.goal.mode = self.goal.MODE_VEL
		# self.goal.waypointType = DISABLE
		self.recent_goal = QuadGoal()
		self.pose = Pose()
		self.pubGoal = rospy.Publisher('goal', QuadGoal, queue_size=1)


		self.demo_mode = rospy.get_param('~demo', False)
		self.att_thrust_mode = rospy.get_param('~joyAttitude', False)

		self.spinup_time = rospy.get_param('cntrl/spinup_time')

		# Maximum desired acceleration
		self.a_max = rospy.get_param('speed_tester/a_max')

		# Maximum desired velocity
		self.v_max_param = rospy.get_param('speed_tester/v_max')
		self.v_max = self.v_max_param
		self.x_start = 0
		self.x_stop = 0
		self.y_start = 2
		self.y_stop = -6

		self.stop = False		

		# These should be params
		self.alt = 0.5
		self.v = 0
		self.v_cmd = 0
		self.v0 = 0
		self.a0 = 0
		self.j0 = 0
		self.v_rate = 0.08
		self.yaw = -np.pi/2
		self.stop_distance = 1.1*self.v_max**2/(self.v_rate/CONTROL_DT)/2

		self.go = False
		self.v_state = READY

		rospy.Timer(rospy.Duration(CONTROL_DT),self.cmdTimer)

		rospy.loginfo("Initialized")


	def check_peak_speed(self):
		self.L = np.abs(self.y_start-self.y_stop)
		self.calc_stopping_distance()
		if self.L  <= 2*self.d_stop:
			rospy.logerr("Path not long enough to reach %0.2f m/s.", self.v_max)
			d_stop = self.L/2
			peak_vel = np.sqrt(2*d_stop*self.a_max/1.875)
			rospy.logerr("Changing peak velocity to: %0.2f m/s.",peak_vel)
			self.v_max = peak_vel
		else:
			if self.v_max != self.v_max_param:
				rospy.logwarn("Changing back to max velocity.")
			self.v_max = self.v_max_param

	def calc_stopping_distance(self):
		T = 1.875*np.abs(self.v_max)/self.a_max

		A = np.array([[0,0,0,0,0,1],
					 [0,0,0,0,1,0], 
					 [0,0,0,2,0,0], 
					 [T**5, T**4, T**3, T**2, T, 1], 
					 [5*T**4, 4*T**3, 3*T**2, 2*T, 1, 0], 
					 [20*T**3, 12*T**2, 6*T, 2, 0, 0]])

		b = np.array([self.v_max,0,0,0,0,0])[None].T

		v_coeff = np.dot(np.linalg.inv(A),b).T[0]

		s_coeff = np.hstack((np.array([1./6, 1./5, 1./4, 1./3, 1./2, 1])*v_coeff,0))

		self.d_stop = utils.polyVal(s_coeff,T)


	def calc_vel(self,v0,a0,j0):
		# Starting phase
		if self.v_state == START:
			# Find T based on v0, v_max, and a_max
			T = 1.875*np.abs(self.v_max-v0)/self.a_max

			A = np.array([[0,0,0,0,0,1],
						 [0,0,0,0,1,0], 
						 [0,0,0,2,0,0], 
						 [T**5, T**4, T**3, T**2, T, 1], 
						 [5*T**4, 4*T**3, 3*T**2, 2*T, 1, 0], 
						 [20*T**3, 12*T**2, 6*T, 2, 0, 0]])

			b = np.array([v0,a0,j0,self.v_max,0,0])[None].T

			self.v_coeff = np.dot(np.linalg.inv(A),b).T[0]
			self.a_coeff = (np.array([5,4,3,2,1,0])*self.v_coeff)[:-1]
			self.j_coeff = (np.array([4,3,2,1,0])*self.a_coeff)[:-1]
			# self.start_time = rospy.get_time()

		# Nominal stop
		elif self.v_state == STOP:
			# Find T based on v0, v_max, and a_max
			T = 1.875*np.abs(0-v0)/self.a_max

			A = np.array([[0,0,0,0,0,1],
						 [0,0,0,0,1,0], 
						 [0,0,0,2,0,0], 
						 [T**5, T**4, T**3, T**2, T, 1], 
						 [5*T**4, 4*T**3, 3*T**2, 2*T, 1, 0], 
						 [20*T**3, 12*T**2, 6*T, 2, 0, 0]])

			b = np.array([v0,a0,j0,0,0,0])[None].T

			self.v_coeff = np.dot(np.linalg.inv(A),b).T[0]
			self.a_coeff = (np.array([5,4,3,2,1,0])*self.v_coeff)[:-1]
			self.j_coeff = (np.array([4,3,2,1,0])*self.a_coeff)[:-1]

		# Perform emergency stop 
		elif self.v_state == ESTOP:
			# Find T based on v0 and distance d to last WP 
			T = 2*self.d/v0

			A = np.array([[0,0,0,0,0,1],
						 [0,0,0,0,1,0], 
						 [0,0,0,2,0,0], 
						 [T**5, T**4, T**3, T**2, T, 1], 
						 [5*T**4, 4*T**3, 3*T**2, 2*T, 1, 0], 
						 [20*T**3, 12*T**2, 6*T, 2, 0, 0]])

			b = np.array([v0,a0,j0,0,0,0])[None].T

			self.v_coeff = np.dot(np.linalg.inv(A),b).T[0]
			self.a_coeff = (np.array([5,4,3,2,1,0])*self.v_coeff)[:-1]
			self.j_coeff = (np.array([4,3,2,1,0])*self.a_coeff)[:-1]
		
		# In cruise mode
		else:
			self.v_coeff = np.hstack((np.zeros(len(self.v_coeff)-1),self.v_max))
			self.a_coeff = np.zeros(len(self.v_coeff))[:-1]
			self.j_coeff = np.zeros(len(self.v_coeff))[:-2]

	def check_status(self,p,v_cmd):
		# Check to see if we're near cruise velocity. If so, enter cruise state
		if v_cmd > self.v_max and not (self.v_state==CRUISE) and not (self.v_state==STOP):
			rospy.loginfo("In Cruise Phase")
			self.v_state = CRUISE
			self.calc_vel(self.v0,self.a0,self.j0)
		elif (self.v_state==READY) and self.go:
			rospy.loginfo("In Starting Phase")
			self.v_state=START
			self.calc_vel(self.v0,self.a0,self.j0)
			self.start_time = rospy.get_time()
		
		if (self.d < self.d_stop and (self.v_state==CRUISE)):
			rospy.loginfo("In Stopping Phase")
			self.v_state = STOP
			self.calc_vel(self.v0,self.a0,self.j0)
			self.stop_time = rospy.get_time()


	def poseCB(self, data):
		self.pose = data.pose

	def velCB(self, data):
		self.vel = data.twist.linear

	def goalCB(self, data):
		self.recent_goal = data

	def sendGoal(self):
		# self.goal.waypointType = self.wpType
		mode = QuadGoal()
		# if self.go:
		# 	self.goal.mode = mode.MODE_VEL
		# else:
		# 	self.goal.mode = mode.MODE_POS

		if self.wpType == DISABLE:
			self.goal.cut_power = True
		else:
			self.goal.cut_power = False

		self.goal.header.stamp = rospy.get_rostime()
		self.pubGoal.publish(self.goal)

	def joyCB(self, data):
		if self.status == NOT_FLYING:
			self.goal.yaw = utils.quat2yaw(self.pose.orientation)
	    
	    # takeoff
		if data.buttons[A] and self.status == NOT_FLYING:
			if self.status!=TAKEOFF:                
				rospy.loginfo("Waiting for spinup...")

				# set initial goal to current pose
				self.goal.pos = copy.copy(self.pose.position)
				self.goal.vel.x = self.goal.vel.y = self.goal.vel.z = 0
				self.goal.yaw = utils.quat2yaw(self.pose.orientation)

				self.wpType = TAKEOFF

				rospy.sleep(self.spinup_time)

				self.status = TAKEOFF
				rospy.loginfo("Taking Off")
			
				self.y_start = self.goal.pos.y

				if self.test==SMOOTH or self.test==SMOOTH_FF:
					self.check_peak_speed()


	        # if self.demo_mode:
	        # self.goal.pos.z = 0.5

	    # emergency disable
		elif data.buttons[B] and self.status != NOT_FLYING:
			rospy.loginfo("Killing")
			self.status = NOT_FLYING
			self.wpType = DISABLE
			self.go = False


		# landing
		elif data.buttons[X] and self.status == FLYING:
			rospy.loginfo("Landing")

			self.status = LANDING
			# self.wpType = LAND
			self.goal.vel.x = 0
			self.goal.vel.y = 0
			self.goal.vel.z = 0
			self.goal.dyaw = 0
			self.go = False

		elif data.buttons[CENTER] and self.status == FLYING:
			self.goal.yaw = self.yaw
			self.goal.dyaw = 0
			rospy.loginfo("Ready")

		elif data.buttons[7] and self.status == FLYING:
			rospy.loginfo("Starting")
			self.go = True


	def cmdTimer(self,e):	
		if self.go:

			self.d = np.linalg.norm(np.array([self.pose.position.x - self.x_stop,self.pose.position.y-self.y_stop]))

			if self.test==STEP:
				if self.d > 0.5 and not self.stop:
					self.goal.pos = copy.copy(self.pose.position)
					self.goal.pos.z = self.alt
					self.goal.vel.x = 0
					self.goal.vel.y = -self.v_max
					self.goal.vel.z = 0
				else:
					self.stop = False
					self.goal.pos = copy.copy(self.pose.position)
					# self.goal.pos.x = 0
					# self.goal.pos.y = self.y_stop
					self.goal.pos.z = self.alt
					self.goal.vel.x = 0
					self.goal.vel.y = 0
					self.goal.vel.z = 0
					self.go = False

					rospy.loginfo("Done")


			elif self.test==SMOOTH or self.test==SMOOTH_FF:
				p = np.array([self.pose.position.x,self.pose.position.y,self.pose.position.z])
				self.check_status(p,self.v_cmd)

				if (self.v_state==START) or (self.v_state==CRUISE):
					self.t = rospy.get_time() - self.start_time
					self.v_cmd = utils.polyVal(self.v_coeff,self.t)
					self.at_cmd = utils.polyVal(self.a_coeff,self.t)
					self.jt_cmd = utils.polyVal(self.j_coeff,self.t)

				elif (self.v_state==STOP) or (self.v_state==ESTOP):
					self.t = rospy.get_time() - self.stop_time
					self.v_cmd = utils.polyVal(self.v_coeff,self.t)
					self.at_cmd = utils.polyVal(self.a_coeff,self.t)
					self.jt_cmd = utils.polyVal(self.j_coeff,self.t)

				self.v0 = self.v_cmd
				self.a0 = self.at_cmd
				self.j0 = self.jt_cmd
				
				self.goal.vel.x = 0
				self.goal.vel.y = -self.v_cmd
				self.goal.vel.z = 0

				if self.goal.mode==self.goal.MODE_POS:
					self.goal.pos.x = self.goal.pos.x + self.goal.vel.x*CONTROL_DT
					self.goal.pos.y = self.goal.pos.y + self.goal.vel.y*CONTROL_DT
				else:
					self.goal.pos = copy.copy(self.pose.position)

				self.goal.pos.z = self.alt

				# Feedforward acceleration and jerk
				if self.test==SMOOTH_FF:
					self.goal.accel.x =  0
					self.goal.accel.y =  -self.at_cmd
					self.goal.accel.z =  0

					self.goal.jerk.x =  0
					self.goal.jerk.y =  -self.jt_cmd
					self.goal.jerk.z =  0

				if self.v_cmd < 0.001 and (self.v_state==STOP or self.v_state==ESTOP):
					self.goal.pos = copy.copy(self.pose.position)				
					self.goal.pos.z = self.alt
					self.goal.vel.x = 0
					self.goal.vel.y = 0
					self.goal.vel.z = 0
					self.goal.accel.x = 0
					self.goal.accel.y = 0
					self.goal.accel.z = 0
					self.goal.jerk.x = 0
					self.goal.jerk.y = 0
					self.goal.jerk.z = 0

					self.go = False
					self.v_state = READY

					rospy.loginfo("Done")


			# self.t = rospy.get_time() - self.start_time

			# rospy.loginfo(self.t)
			# if self.t > self.tf:
			# 	self.t = self.tf

			# if self.t < self.tf:
			# 	self.y = np.polyval(self.Coeff,self.t)
			# 	v_coeff = np.polyder(self.Coeff)
			# 	a_coeff = np.polyder(v_coeff)
			# 	j_coeff = np.polyder(a_coeff)

			# 	self.vt_calc = np.polyval(v_coeff,self.t)
			# 	self.at_calc = np.polyval(a_coeff,self.t)
			# 	self.jt_calc = np.polyval(j_coeff,self.t)


			# else:
			# 	self.y = self.y_stop
			# 	self.vt_calc = 0
			# 	self.at_calc = 0
			# 	self.jt_calc = 0

			# T = np.array([np.cos(self.theta), np.sin(self.theta), 0])
			# B = np.array([0,0,1])
			# N = np.cross(T,B)

			# at = self.at_calc
			# an = self.vt_calc**2*self.K

			# jt = -self.vt_calc**3*self.K**2 + self.jt_calc
			# jn = self.vt_calc**2*self.Kd + 3*self.K*self.vt_calc*self.at_calc

			# self.goal.pos.x = self.x
			# self.goal.pos.y = self.y
			# self.goal.pos.z = self.alt
			# self.goal.vel.x = 0
			# self.goal.vel.y = self.vt_calc
			# self.goal.vel.z = 0
			# self.goal.accel.x = 0
			# self.goal.accel.y = self.at_calc
			# self.goal.jerk.x = 0
			# self.goal.jerk.y = self.jt_calc
			# self.goal.vel.x = self.vt_calc*T[0]
			# self.goal.vel.y = self.vt_calc*T[1]
			# self.goal.vel.z = 0
			# self.goal.accel.x = at*T[0] + an*N[0]
			# self.goal.accel.y = at*T[1] + an*N[1]
			# self.goal.jerk.x = jt*T[0] + jn*N[0]
			# self.goal.jerk.y = jt*T[1] + jn*N[1]

		if self.status == LANDING:
			if self.pose.position.z > 0.4:
				# fast landing
				self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.0035, 2.0, -0.1)
			else:
				# slow landing
				self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.001, 2.0, -0.1)
			if self.goal.pos.z == -0.1:
				self.status = NOT_FLYING
				self.wpType = DISABLE
				rospy.loginfo("Landed!")

		if self.status == TAKEOFF:
			self.goal.pos.z = utils.saturate(self.goal.pos.z+0.0035, self.alt,-0.1)
			if (np.abs(self.alt-self.pose.position.z) < 0.1 and self.goal.pos.z==self.alt):
				self.status = FLYING
				rospy.loginfo("Takeoff complete!")

		if self.transmitting:
			self.sendGoal()


if __name__ == '__main__':
	ns = rospy.get_namespace()
	try:
		rospy.init_node('path_gen')
		if str(ns) == '/':
			rospy.logfatal("Need to specify namespace as vehicle name.")
			rospy.logfatal("This is tyipcally accomplished in a launch file.")
			rospy.logfatal("Command line: ROS_NAMESPACE=RQ01 rosrun fla speed_test.py")
		else:
			rospy.loginfo("Starting joystick teleop node for: " + ns)
			c = speedTest()
			rospy.Subscriber("joy", Joy, c.joyCB)
			rospy.Subscriber("vicon", ViconState, c.poseCB)
			rospy.Subscriber("goal", QuadGoal, c.goalCB)
			rospy.spin()   
	except rospy.ROSInterruptException:
		pass