#!/usr/bin/env python
# cartesian joystick waypoint control for quadrotor
import roslib
import rospy
import copy
import math
import numpy as np
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped, Pose, TwistStamped, Twist

# local imports
from acl_msgs.msg import QuadGoal
from acl_msgs.msg import JoyDef
from aclpy import utils

import accel as ac

controlDT = 1./100

NOT_FLYING = 0
FLYING = 1
LANDING = 2

TAKEOFF = 1
DISABLE = 2
RESET_INTEGRATORS = 4

class speedTest:

	def __init__(self):
		self.vm = 14
		self.am = 20
		self.theta = -np.pi/2
		self.yaw = -np.pi/2
		self.x = 0
		self.y_start = 2.6
		self.y_stop = -6
		self. L = self.y_start-self.y_stop
		self.alt = 1

		self.K = 0
		self.Kd = 0
		self.s = 0.01
		self.vt_calc = 0

		self.m = 0.4
		#Drag model
		self.d1 = 0.024
		# GPOPS start
		self.a2 = 0.0258 
		self.a1 = 0.0442
		# GPOPS stop
		self.b2 = 0.0181
		self.b1 = 0.124

		self.tf = 2.182*self.L/self.vm

		A = np.matrix([[1,0,0,0,0,0,0,0],
						[0,1,0,0,0,0,0,0],
						[0,0,1,0,0,0,0,0],
						[0,0,0,1,0,0,0,0], 
						[1, self.tf, self.tf**2, self.tf**3, self.tf**4, self.tf**5, self.tf**6, self.tf**7],
						[0, 1, 2*self.tf, 3*self.tf**2, 4*self.tf**3, 5*self.tf**4, 6*self.tf**5, 7*self.tf**6],
				        [0, 0, 2, 6*self.tf, 12*self.tf**2, 20*self.tf**3, 30*self.tf**4, 42*self.tf**5],
				        [0, 0, 0, 6, 24*self.tf, 60*self.tf**2, 120*self.tf**3, 210*self.tf**4]])

		b = np.matrix([[self.y_start],[0],[0],[0],[self.y_stop],[0],[0],[0]]) # a(0) a(sf) v(sf) j(0) j(sf) u(0)
		temp = np.linalg.inv(A)*b
		temp = np.asarray(temp.T)[0][::-1]
		self.Coeff = temp

		self.first = True

		self.at2jtcoeff = np.arange(5,-1,-1)
		self.at2vtcoeff = 1./np.arange(6,0,-1)

		self.status = NOT_FLYING
		self.transmitting = True
		self.wpType = DISABLE
		self.start = False
		
		self.goal = QuadGoal()
		self.goal.waypointType = DISABLE
		self.recent_goal = QuadGoal()
		self.pose = Pose()
		self.vel = Twist()
		self.joyinfo = JoyDef()
		self.pubGoal = rospy.Publisher('goal', QuadGoal, queue_size=1)

		rospy.Timer(rospy.Duration(controlDT),self.cmdTimer)

	def poseCB(self, data):
		self.pose = data.pose

	def velCB(self, data):
		self.vel = data.twist.linear

	def goalCB(self, data):
		self.recent_goal = data

	def sendGoal(self):
		self.goal.waypointType = self.wpType
		self.pubGoal.publish(self.goal)

	def joyCB(self, data):

		if self.status == NOT_FLYING:
			self.goal.yaw = utils.quat2yaw(self.pose.orientation)

		if data.buttons[self.joyinfo.A] and self.status == NOT_FLYING:
			self.status = FLYING
			self.wpType = TAKEOFF

			# set initial goal to current pose
			self.goal.pos = copy.copy(self.pose.position)
			self.goal.vel.x = self.goal.vel.y = self.goal.vel.z = 0
			self.goal.yaw = utils.quat2yaw(self.pose.orientation)
			self.goal.dyaw = 0
				
			
		# emergency disable
		elif data.buttons[self.joyinfo.B] and self.status != NOT_FLYING:
			self.status = NOT_FLYING
			self.wpType = DISABLE
			self.start = False

		# landing
		elif data.buttons[self.joyinfo.X] and self.status == FLYING:
			self.start = False
			self.status = LANDING
			# self.wpType = LAND
			self.goal.pos.x = self.pose.position.x
			self.goal.pos.y = self.pose.position.y
			self.goal.vel.x = 0
			self.goal.vel.y = 0
			self.goal.vel.z = 0 
			self.goal.dyaw = 0
			self.I = 0

		elif data.buttons[self.joyinfo.CENTER] and self.status == FLYING:
			self.goal.pos.x = self.x
			self.goal.pos.y = self.y_start
			self.goal.pos.z = self.alt
			self.goal.yaw = self.yaw
			self.goal.dyaw = 0
			rospy.loginfo("Ready")
			
		elif data.buttons[self.joyinfo.START] and self.status == FLYING:
			rospy.loginfo("Starting")
			self.start = True
			self.start_time = rospy.get_time()

		if self.status == TAKEOFF:
			self.goal.pos.z = utils.saturate(self.goal.pos.z+0.0035, self.alt,-0.1)

		if self.status == LANDING:
			if self.pose.position.z > 0.4:
				# fast landing
				self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.0035, 2.0, -0.1)
			else:
				# slow landing
				self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.001, 2.0, -0.1)
			if self.goal.pos.z == -0.1:
				self.status = NOT_FLYING
				self.wpType = DISABLE
				rospy.loginfo("Landed!")


	def cmdTimer(self,e):	
		if self.start:
			
			self.t = rospy.get_time() - self.start_time

			rospy.loginfo(self.t)
			if self.t > self.tf:
				self.t = self.tf

			if self.t < self.tf:
				self.y = np.polyval(self.Coeff,self.t)
				v_coeff = np.polyder(self.Coeff)
				a_coeff = np.polyder(v_coeff)
				j_coeff = np.polyder(a_coeff)

				self.vt_calc = np.polyval(v_coeff,self.t)
				self.at_calc = np.polyval(a_coeff,self.t)
				self.jt_calc = np.polyval(j_coeff,self.t)


			else:
				self.y = self.y_stop
				self.vt_calc = 0
				self.at_calc = 0
				self.jt_calc = 0

			T = np.array([np.cos(self.theta), np.sin(self.theta), 0])
			B = np.array([0,0,1])
			N = np.cross(T,B)

			at = self.at_calc
			an = self.vt_calc**2*self.K

			jt = -self.vt_calc**3*self.K**2 + self.jt_calc
			jn = self.vt_calc**2*self.Kd + 3*self.K*self.vt_calc*self.at_calc

			self.goal.pos.x = self.x
			self.goal.pos.y = self.y
			self.goal.pos.z = self.alt
			self.goal.vel.x = 0
			self.goal.vel.y = self.vt_calc
			self.goal.vel.z = 0
			self.goal.accel.x = 0
			self.goal.accel.y = self.at_calc
			self.goal.jerk.x = 0
			self.goal.jerk.y = self.jt_calc
			# self.goal.vel.x = self.vt_calc*T[0]
			# self.goal.vel.y = self.vt_calc*T[1]
			# self.goal.vel.z = 0
			# self.goal.accel.x = at*T[0] + an*N[0]
			# self.goal.accel.y = at*T[1] + an*N[1]
			# self.goal.jerk.x = jt*T[0] + jn*N[0]
			# self.goal.jerk.y = jt*T[1] + jn*N[1]

		self.sendGoal()

if __name__ == '__main__':
	ns = rospy.get_namespace()
	try:
		rospy.init_node('path_gen')
		if str(ns) == '/':
			rospy.logfatal("Need to specify namespace as vehicle name.")
			rospy.logfatal("This is tyipcally accomplished in a launch file.")
			rospy.logfatal("Command line: ROS_NAMESPACE=RQ01 rosrun fla speed_test.py")
		else:
			rospy.loginfo("Starting joystick teleop node for: " + ns)
			c = speedTest()
			rospy.Subscriber("/joy", Joy, c.joyCB)
			rospy.Subscriber("pose", PoseStamped, c.poseCB)
			rospy.Subscriber("vel", TwistStamped, c.velCB)
			rospy.Subscriber("goal", QuadGoal, c.goalCB)
			rospy.spin()   
	except rospy.ROSInterruptException:
		pass