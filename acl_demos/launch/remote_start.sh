#!/bin/bash
VEH=$1
NUM=$2
QUAD=$VEH$NUM
SESSION=remote_$QUAD

if [ -z "$1" ]
	then
		echo "No quad selected"
		echo "Example use: ./remote_start.sh SQ 02"
		exit
	else
		echo "Starting quad $QUAD"
fi

tmux -2 new-session -d -s $SESSION
# tmux set pan-boarder-status top

tmux split-window -h
tmux split-window -v
tmux select-pane -t 0
tmux split-window -v
tmux select-pane -t 2
tmux split-window -v

for _pane in $(tmux list-pane -F '#P'); do
	# ssh into vehicle using env variables from cfg file
	tmux send-keys -t ${_pane} "ssh root@$QUAD.local" C-m
	sleep 1
	tmux send-keys -t ${_pane} "source /home/linaro/.bashrc" C-m
	tmux send-keys -t ${_pane} "cd" C-m
done

tmux send-keys -t 0 "ntpdate -u 192.168.0.19" C-m
tmux send-keys -t 3 "imu_app -s 2" C-m
tmux send-keys -t 4 "roslaunch snap snap.launch veh:=$VEH num:=$NUM" C-m
tmux send-keys -t 0 "roslaunch system_launch quad.launch veh:=$VEH num:=$NUM" C-m
tmux send-keys -t 1 "roslaunch cvx cvx.launch quad:=$QUAD" C-m
tmux send-keys -t 2 "sleep 10; ./pwm_interface" C-m

tmux -2 attach-session -t $SESSION
