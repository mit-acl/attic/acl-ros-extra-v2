#!/usr/bin/env python
import rospy
import tf
from visualization_msgs.msg import Marker, MarkerArray
from acl_msgs.msg import VehicleList


class Visualizer(object):
    def __init__(self):
        self.node_name = rospy.get_name()
        self.markers = dict()
        self.transformer = tf.TransformListener()

        # Read Parameters
        self.veh_msg_types = rospy.get_param("~veh_msg_types","acl_msgs/ViconState")
        self.pub_period = rospy.get_param("~pub_period",1.0)
        self.veh_models = rospy.get_param("~veh_models")
        self.veh_type_list = self.veh_models.keys()

        # Setup publisher
        self.pub_markerarray = rospy.Publisher("~vehicle_markers",MarkerArray,queue_size=1)
        self.pub_veh_list = rospy.Publisher("~vehicle_list",VehicleList,queue_size=1)

        self.timer_pub = rospy.Timer(rospy.Duration(self.pub_period),self.cbTimerPub)

    def genMarkersByVehNameList(self,veh_name_list):
        for veh_name in veh_name_list:
            veh_type = self.getVehType(veh_name)
            # Skip the veh if not defined in veh_models. See visualizer.yaml
            if veh_type not in self.veh_models.keys():
                # rospy.loginfo("[%s] %s's type is not recognized. Excluded from vehicle_list." %(rospy.get_name(),veh_name))
                continue
            # Add if doesn't have a marker already
            if veh_name not in self.markers.keys():
                self.addVeh(veh_name)
        
        # Remove marker from vehicle that's no longer there
        for veh_name in self.markers.keys():
            if veh_name not in veh_name_list:
                self.markers.pop(veh_name)

    def extractVehName(self,topic_name):
        names = topic_name.split("/")
        if len(names) < 2:
            return None
        return names[1]

    def getVehNameList(self):
        # Grab all topics
        topics_and_types = rospy.get_published_topics(namespace='/')
        vehicle_names = list()

        now = rospy.Time.now()

        for (topic, topic_type) in topics_and_types:
            if topic_type in self.veh_msg_types:
                veh_name = self.extractVehName(topic)
                if veh_name in vehicle_names:
                    continue

                # Only add vehicle name if the tf is not too old. frameExsit doesnt' quite work. (Alwasy exist if it shows up once.)
                try:
                    frame_comm_time = self.transformer.getLatestCommonTime("vicon",veh_name)
                except tf.Exception:
                    continue

                if (now - frame_comm_time).to_sec() <= 2.0:
                    vehicle_names.append(veh_name)

        return vehicle_names

    def cbTimerPub(self, event):
        now = rospy.Time.now()
        # Extract vehicle names from topics
        veh_name_list = self.getVehNameList()
        veh_list_msg = VehicleList()
        veh_list_msg.header.stamp = now
        veh_list_msg.vehicle_names = veh_name_list
        self.pub_veh_list.publish(veh_list_msg)

        # Generate markers according to vehicle name list
        self.genMarkersByVehNameList(veh_name_list)
        marker_array = MarkerArray()
        for veh_name, marker in self.markers.items():
            marker.header.stamp = now
            marker_array.markers.append(marker)
        self.pub_markerarray.publish(marker_array)

    def addVeh(self, veh_name):
        # marker.mesh_use_embedded_materials = True
        self.markers[veh_name] = self.getVehMarker(veh_name)

    def getVehType(self, veh_name):
        return veh_name[0:2]

    def isSimulated(self, veh_name):
        return veh_name[-1] == "s"

    def getVehMarker(self, veh_name):
        # Load model spec
        model_spec_dict = self.veh_models[self.getVehType(veh_name)]

        # Construct Marker
        marker = Marker()
        marker.header.frame_id = veh_name
        marker.header.stamp = rospy.Time.now()
        marker.ns = veh_name
        marker.id = 0
        marker.lifetime = rospy.Duration.from_sec(self.pub_period + 0.25)
        marker.action = Marker.ADD
        marker.pose.orientation.w = 1.0
        
        # Look up color
        color_dict = model_spec_dict['color']
        marker.color.r = color_dict['r']
        marker.color.g = color_dict['g']
        marker.color.b = color_dict['b']
        marker.color.a = color_dict['a']
        
        # Set transparent if simulated
        if self.isSimulated(veh_name):
            marker.color.a = 0.8

        # Look look up type
        if model_spec_dict["use_mesh"]:
            marker.type = Marker.MESH_RESOURCE
            marker.mesh_resource= "package://acl_rviz/models/" + model_spec_dict["mesh_file"]
        else:
            marker.type = Marker.CUBE

        # Look up scale
        scale = model_spec_dict['scale']
        marker.scale.x = scale
        marker.scale.y = scale
        marker.scale.z = scale

        marker.frame_locked = True
        return marker

if __name__ == '__main__':
    rospy.init_node('visualizer')
    node = Visualizer()
    rospy.spin()
