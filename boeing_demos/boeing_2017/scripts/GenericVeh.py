'''
Created on Apr 6, 2016

@author: Shayegan Omidshafiei
'''

from waypoint_manager.srv import SetNewWaypoint
from geometry_msgs.msg import PoseStamped, Pose
from std_msgs.msg import Int32
from std_srvs.srv import Empty, EmptyRequest 
import numpy as np
import rospy
from acl_msgs.msg import BoolStamped, ImageObj, QuadWaypoint
import threading

class GenericVeh(object): 
    def __init__(self, idx, name, should_use_camera, wpts_major, wpts_minor_delta, wpts_major_obj_seeked):
        self.idx = idx
        self.wpts_major = wpts_major
        self.wpts_minor_delta = wpts_minor_delta
        self.wpts_major_obj_seeked = wpts_major_obj_seeked
        self.isOutputOn = False
        self.numProbDataToCollect = 9
        self.name = name
        self.should_use_camera = int(should_use_camera)
        print 'Initialized ', self.name, ' should_use_camera: ', self.should_use_camera

        # Services
        if self.should_use_camera:
            self.subClassProba = rospy.Subscriber('/obj_all/image_obj', ImageObj, self.cbImageObj)
            self.total_obj_counts = dict()
            self.num_prob_msgs = 0
            self.enable_camera_obs = False
            self.lock_num_prob_msgs = threading.Lock()
            
        rospy.wait_for_service(self.name + '/wpt_go_to_now')
        rospy.wait_for_service(self.name + '/wpt_add')
        # rospy.wait_for_service(self.name + '/wpt_pick_up_pkg_at')
        rospy.wait_for_service(self.name + '/wpt_land_now')
        rospy.wait_for_service(self.name + '/wpt_takeoff')

        self.srv_wpt_go_to_now = rospy.ServiceProxy(self.name + '/wpt_go_to_now', SetNewWaypoint)
        self.srv_wpt_add = rospy.ServiceProxy(self.name + '/wpt_add', SetNewWaypoint)
        # self.srv_wpt_pick_up_pkg_at = rospy.ServiceProxy(self.name + '/wpt_pick_up_pkg_at', SetNewWaypoint)
        self.srv_wpt_land_now = rospy.ServiceProxy(self.name + '/wpt_land_now', Empty)
        self.srv_wpt_takeoff = rospy.ServiceProxy(self.name + '/wpt_takeoff', Empty)
        
        self.latest_wpt_complete = True #used for monitoring when to send new wpt
        self.i_minor_wpt = 0
        self.i_major_wpt = 0
        self.new_wpt_is_minor = False
    
    def setWptToNextMinor(self):
        # Exhausted all minor wpts
        if self.i_minor_wpt >= len(self.wpts_minor_delta):
            self.setWptToNextMajor()
        else:
            self.wpt_new = self.wpts_major[self.i_major_wpt] + self.wpts_minor_delta[self.i_minor_wpt]
            self.new_wpt_is_minor = True
            self.i_minor_wpt += 1

    def setWptToNextMajor(self):
        self.i_minor_wpt = 0
        self.i_major_wpt += 1

        if self.i_major_wpt >= len(self.wpts_major):
            self.i_major_wpt = 0

        self.wpt_new = self.wpts_major[self.i_major_wpt]
        self.new_wpt_is_minor = False
        
    def sendTakeoffCmd(self):
        req = EmptyRequest()
        resp = self.srv_wpt_takeoff()

    def sendGoToNowCmd(self, quadwaypoint):
        req = QuadWaypoint()
        req.point.x = quadwaypoint.point.x
        req.point.y = quadwaypoint.point.y
        req.point.z = quadwaypoint.point.z
        req.heading = quadwaypoint.heading
        resp = self.srv_wpt_go_to_now(req)

    def sendWptAddCmd(self, quadwaypoint):
        req = QuadWaypoint()
        req.point.x = quadwaypoint.point.x
        req.point.y = quadwaypoint.point.y
        req.point.z = quadwaypoint.point.z
        req.heading = quadwaypoint.heading
        resp = self.srv_wpt_add(req)

    def sendLandNowCmd(self):
        req = EmptyRequest()
        self.srv_wpt_land_now()

    def sendPickUpPkgAtCmd(self, quadwaypoint):
        req = QuadWaypoint()
        req.point.x = quadwaypoint.point.x
        req.point.y = quadwaypoint.point.y
        req.point.z = quadwaypoint.point.z
        req.heading = quadwaypoint.heading
        resp = self.srv_wpt_pick_up_pkg_at(req)

    def getClassObsDict(self):
        print '!!!!!! Waiting for', self.name,' observation'
        if (self.should_use_camera):
            self.ClassObsDict = self.getCameraClassObs()
        else:
            # Simulated observation
            rospy.sleep(2.0)
            self.ClassObsDict = dict() # Return null observation by default, to test exploration
            i_rand = np.random.randint(1+len(self.wpts_major_obj_seeked))
            if i_rand < len(self.wpts_major_obj_seeked): # len/(len+1) probability of returning desired observation
                # Sets the dictionary to have the correct count of the desired object
                self.ClassObsDict[self.wpts_major_obj_seeked[i_rand][0]] = self.wpts_major_obj_seeked[i_rand][1]-0.1

        # Round object counts to nearest int
        for key in self.ClassObsDict:
            self.ClassObsDict[key] = np.round(self.ClassObsDict[key])
        print 'Final (rounded) obs:', self.ClassObsDict
        return self.ClassObsDict

    def cbImageObj(self,msg):
        if self.enable_camera_obs:
            print '------------'
            with self.lock_num_prob_msgs:
                # First, count the number of each object detected in latest frame
                new_frame_obj_counts = dict()
                for obj in msg.obj:
                    if obj.class_string not in new_frame_obj_counts:
                        new_frame_obj_counts[obj.class_string] = 1.
                    else:
                        new_frame_obj_counts[obj.class_string] += 1.

                # Objects detected in the latest frame should be incremented in running total
                for class_string in new_frame_obj_counts:
                    # Object never encountered in running total. Initialize its previous count to 0.
                    if class_string not in self.total_obj_counts:
                        self.total_obj_counts[class_string] = 0.

                    # Streaming average number of instances of each class observed
                    self.total_obj_counts[class_string] = (1.*self.num_prob_msgs*self.total_obj_counts[class_string] + new_frame_obj_counts[class_string])/(self.num_prob_msgs+1.)

                # Objects that were previously detected but not detected in latest frame should be decremented
                for class_string in self.total_obj_counts:
                    if class_string not in new_frame_obj_counts:
                        # Streaming average number of instances of each class observed
                        self.total_obj_counts[class_string] = (1.*self.num_prob_msgs*self.total_obj_counts[class_string] + 0.)/(self.num_prob_msgs+1.)

                print self.total_obj_counts
                self.num_prob_msgs+=1
        
    def getNthClassProbaMsg(self):
        with self.lock_num_prob_msgs:
            self.total_obj_counts = dict()
            # Should start fresh count of objects, in case previous objects were seen on the way here
            self.num_prob_msgs = 0
            self.enable_camera_obs = True
        # TODO 2017 should add a timer here, to make sure that if the camera is off for some reason, the quad doesn't stall forever waiting for an obs
        while (self.num_prob_msgs < self.numProbDataToCollect):
            pass
        self.enable_camera_obs = False
        
        return self.total_obj_counts

    # def triggerCaffePublisher(self, enablePublisher):
    #     msg = BoolStamped()
    #     msg.header.stamp = rospy.Time.now()
    #     msg.data = enablePublisher
    #     self.pubCaffeTriggerPublish.publish(msg)

    def getCameraClassObs(self):
        # class_proba_thresh = 0.5
        # self.triggerCaffePublisher(True)
        # print self.name, 'triggering caffe publisher'
        rospy.sleep(0.5)

        try:
            total_obj_counts = self.getNthClassProbaMsg()
            return total_obj_counts
        except:
            return None # Did not receive class_probs msg

        # self.triggerCaffePublisher(False)

        # if np.amax(class_probas) > class_proba_thresh:
        #     # Class probability was high enough - send correct obsIdx so mission can continue
        #     class_best = class_names[np.argmax(class_probas)]
        #     print '!!!!!!!!!!!!', self.name, ' class probabilities:', class_proba_thresh, ', class_names:', class_names, '. Best class: ', class_best
        # else:
        #     # Class probability was too low - send a special obsIdx and try again later
        #     return None