branches:
	- acl_control [quad_master] OLD: [acl-msgs]
	
	- acl_planning [goal-heading] OLD:[safety-mods]
	
	- acl-ros-extra-v2 [gazebo_boeing_2017]
	
	- acl-sensors [cnn-cuda when caffe installed, cnn-nocuda when not]
	
	- acl_system [goal-heading] OLD: [safety_code_boeing]

roslaunch acl_rviz acl_rviz.launch rviz_file:=boeing_rviz.rviz

roslaunch boeing_2017 2017demo_sim.launch veh:=LQ num:=01

roslaunch acl_planning tip.launch quad:=LQ01s

roslaunch boeing_2017 2017demo_mm.launch vehs:=LQ01s
